import requests, json, random, os, time
from KEY_NAME import *


class DbHandle:

    def __init__(self, log, version):
        '''
        init the basic data of DbHandle
        :param serverIp:
        '''
        self.logger = log
        self.domain = 'http://3.140.126.204'
        self.update_url = f'{self.domain}/manager/{version}/configs'
        self.post_iplist_url = f'{self.domain}/manager/iplog'
        self.post_error_log_url = f'{self.domain}/manager/v2/sslog'
        self.error_code, self.success_code = 400, 200
        self.token, self.key_waiting = 'Lightsail', "waiting"
        self.key_msg, self.key_code = 'msg', 'code'
        self.key_status, self.key_group = 'status', 'group'
        self.key_json, self.key_text = "json", "text"
        self.key_config_list, self.key_bw = "config_list", "bw"
        self.key_unknowns_error, self.key_ipv4 = "unknowns error", "ipv4"
        self.get_method, self.post_method = "get".upper(), "post".upper()
        requests.adapters.DEFAULT_RETRIES = 3
        self.session = requests.session()
        self.session.keep_alive = False
        self.bandwidth = 80  # default bw 80

    def base_requests(self, url, retries=3, timeout=10, method="GET", data={}, ret_type="json"):
        response = None
        for i in range(retries):
            try:
                info = {
                    "url": url,
                    "data": data,
                    "retries": retries
                }
                if method == self.get_method:
                    response = self.session.request(method=self.get_method, params=data, timeout=timeout, url=url)
                elif method == self.post_method:
                    response = self.session.request(method=self.post_method, data=data, timeout=timeout, url=url)

                if ret_type == self.key_json:
                    response = response.json()
                elif ret_type == self.key_text:
                    response = response.text
                break
            except Exception as e:
                self.logger.exception(f"requests error details:{info}")
        return response

    def save_status_info(self, status, group_type, signal_strength):
        # 增加信号量的保存
        try:
            json_dict = {
                self.key_status: status,
                self.key_group: group_type,
                self.key_waiting: signal_strength
            }
            with open(STATUS_FILE_NAME, 'w+') as f:
                f.write(json.dumps(json_dict))
            with open(PATH_TO_NGINX_STATUS_FILE, 'w+') as f:
                f.write(json.dumps(json_dict))
        except Exception as e:
            self.logger.exception(f"save status info failed")

    def update_info(self, info_dict, signal_strength):
        # 更新VPS信息
        is_success, types = False, [0, 1]
        status, group_type = 1, random.choice(types)
        info_dict.update({'token': self.token})
        data = info_dict
        msg = "requests timeout"
        response = self.base_requests(url=self.update_url, data=data, method=self.post_method)
        if response:
            group_type, msg = response.get(self.key_group, group_type), response.get(self.key_msg, self.key_unknowns_error)
            code, status = response.get(self.key_code, self.error_code), response.get(self.key_status, status)
            if code == self.success_code:
                is_success = True
        if not is_success:
            self.logger.error(f"update vps info failed, reason:{msg}")
        self.save_status_info(status=status, group_type=group_type, signal_strength=signal_strength)
        return is_success

    def query_config(self):
        # 获取服务器配置信息
        config_list, data = [], {'token': self.token}
        response = self.base_requests(url=self.update_url, data=data, method=self.get_method)
        msg, is_success = "requests timeout", False
        if response:
            msg = response.get(self.key_msg, self.key_unknowns_error)
            if self.key_config_list in response:
                config_list = response.get(self.key_config_list, [])
                self.reflush_local_porxy_config(online_proxy=response)
                is_success = True
            if self.key_bw in response:
                self.bandwidth = response.get(self.key_bw, 80)
        if not is_success:
            self.logger.error(f"get config list failed, reason:{msg}")
        return config_list

    def get_bandwidth(self):
        self.logger.info("get bw:{}".format(self.bandwidth))
        return self.bandwidth

    def get_MyIp(self):
        # 获取本机外网ip
        local_proxy = self.load_local_proxy_config()
        ipv4 = local_proxy.get(self.key_ipv4, None) if local_proxy else None
        if ipv4 is None:
            online_config_list = self.query_config()
            ipv4 = online_config_list.get(self.key_ipv4, None) if online_config_list else None
        self.logger.info(f"get machine ipv4:{ipv4}")
        return ipv4

    def post_iplist(self, iplist):
        # 上传IP列表
        is_success = False
        data = {'token': self.token, 'iplist': iplist}
        response = self.base_requests(url=self.post_iplist_url, data=data)
        if response:
            is_success = True
        self.logger.info(f"post iplist response:{response}")
        return is_success

    def post_errorLog(self, port):
        # 上传错误信息
        data = {'token': self.token, "port": str(port), "tag": "lightsail"}
        response = self.base_requests(url=self.post_error_log_url, data=data)
        self.logger.info(f"post errorlog response:{response}")

    def load_local_proxy_config(self):
        ''' 增加重试机制去获取 '''
        retries = 0
        while True:
            proxy, is_success = {}, False
            config_list, config_length = [], 0
            if not os.path.exists(PATH_TO_PROXY_CONFIG):
                self.logger.error("local proxy config not found")
            else:
                with open(PATH_TO_PROXY_CONFIG, "r", encoding="utf-8") as f:
                    try:
                        proxy = json.loads(f.read())
                        config_list = proxy.get(self.key_config_list, [])
                        config_length = len(config_list)
                        if self.key_ipv4 in proxy and config_length > 0:
                            self.logger.info("load proxy config success")
                            is_success = True
                    except Exception as e:
                        self.logger.exception("load local proxy config failed")

            if not is_success:
                # 加载失败的话 则重新从网络中获取加载
                self.query_config()
            else:
                if config_length > 0:
                    return proxy
            sleep_time = 2 ** retries
            time.sleep(sleep_time)
            self.logger.info(f"get local proxy failed , so get online proxy, retries:{retries}, sleep {sleep_time}s")
            retries += 1

    def reflush_local_porxy_config(self, online_proxy):
        # 刷新本地配置文件
        config_list = online_proxy.get(self.key_config_list, [])
        config_length = len(config_list)
        if config_length > 0:
            with open(PATH_TO_PROXY_CONFIG, "w+", encoding="utf-8") as f:
                str_proxy = json.dumps(online_proxy)
                f.write(str_proxy)
                f.flush()
            self.logger.info("refush local proxy success")
        else:
            self.logger.error(f"refush local proxy failed, online_proxy:{online_proxy}")
