deploy_new_control(){
  pushd /
  git clone -b sdk https://545740896%40qq.com:Chanli159753123@gitlab.com/Chanli520/vpn_control.git
  apt install -y python3-pip
  pip3 install loguru==0.5.3
  pip3 install PyYAML==5.4.1
  pip3 install requests==2.25.1
  pip3 install psutil==5.8.0
  pip3 install lxml==4.6.2
  pip3 install pybloom-live
  mkdir /var/log/download
  mkdir /var/log/google
  mkdir /var/log/manager
  mkdir /var/log/ss
  mkdir /etc/supervisor_configs
  cp /vpn_control/supervisor_configs/*.conf /etc/supervisor_configs/
  supervisorctl restart all
}

delete_old_ss_services(){
  service ss_2 stop
  if [ -f "/lib/systemd/system/ss_2.service" ];then
    rm /lib/systemd/system/ss_2.service
    echo "remove ss_2.service  success"
  else
    echo "Old Version not exists"
fi
  systemctl daemon-reload
}

youhua(){
  cat > /etc/sysctl.d/local.conf <<EOF
#max open files
fs.file-max = 196605
#max read buffer
net.core.rmem_max = 67108864
#max write buffer
net.core.wmem_max = 67108864
#default read buffer
net.core.rmem_default = 65536
#default write buffer
net.core.wmem_default = 65536
#max processor input queue
net.core.netdev_max_backlog = 4096
#max backlog
net.core.somaxconn = 4096
#resist SYN flood attacks
net.ipv4.tcp_syncookies = 1
#reuse timewait sockets when safe
net.ipv4.tcp_tw_reuse = 1
#turn off fast timewait sockets recycling
net.ipv4.tcp_tw_recycle = 0
#short FIN timeout
net.ipv4.tcp_fin_timeout = 30
#short keepalive time
net.ipv4.tcp_keepalive_time = 1200
#outbound port range
net.ipv4.ip_local_port_range = 10000 65000
#max SYN backlog
net.ipv4.tcp_max_syn_backlog = 4096
#max timewait sockets held by system simultaneously
net.ipv4.tcp_max_tw_buckets = 5000
#turn on TCP Fast Open on both client and server side
net.ipv4.tcp_fastopen = 3
#TCP receive buffer
net.ipv4.tcp_rmem = 4096 87380 67108864
#TCP write buffer
net.ipv4.tcp_wmem = 4096 65536 67108864
#turn on path MTU discovery
net.ipv4.tcp_mtu_probing = 1
net.ipv4.tcp_congestion_control = bbr
EOF
  sysctl --system
  cat > /etc/sysctl.conf <<EOF
kernel.panic=10
fs.file-max = 196605
EOF
  sysctl -p
  cat > /etc/security/limits.conf <<EOF
* soft nproc 196605
* hard nproc 196605
* soft nofile 196605
* hard nofile 196605
root soft nproc 196605
root hard nproc 196605
root soft nofile 196605
root hard nofile 196605
EOF
  cat >> /etc/pam.d/common-session <<EOF
# here are the per-package modules (the "Primary" block)
session [default=1]                     pam_permit.so
# here's the fallback if no module succeeds
session requisite                       pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
session required                        pam_permit.so
# The pam_umask module will set the umask according to the system default in
# /etc/login.defs and user settings, solving the problem of different
# umask settings with different shells, display managers, remote sessions etc.
# See "man pam_umask".
session optional                        pam_umask.so
# and here are more per-package modules (the "Additional" block)
session required        pam_unix.so
session required        pam_limits.so
session optional        pam_systemd.so
# end of pam-auth-update config
EOF
  cat >> /etc/pam.d/common-session-noninteractive <<EOF
# here are the per-package modules (the "Primary" block)
session [default=1]                     pam_permit.so
# here's the fallback if no module succeeds
session requisite                       pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
session required                        pam_permit.so
# The pam_umask module will set the umask according to the system default in
# /etc/login.defs and user settings, solving the problem of different
# umask settings with different shells, display managers, remote sessions etc.
# See "man pam_umask".
session optional                        pam_umask.so
# and here are more per-package modules (the "Additional" block)
session required        pam_unix.so
session required        pam_limits.so
# end of pam-auth-update config
EOF
  cat >> /etc/systemd/user.conf <<EOF
[Manager]
#LogLevel=info
#LogTarget=console
#LogColor=yes
#LogLocation=no
#SystemCallArchitectures=
#TimerSlackNSec=
#StatusUnitFormat=description
#DefaultTimerAccuracySec=1min
#DefaultStandardOutput=inherit
#DefaultStandardError=inherit
#DefaultTimeoutStartSec=90s
#DefaultTimeoutStopSec=90s
#DefaultTimeoutAbortSec=
#DefaultRestartSec=100ms
#DefaultStartLimitIntervalSec=10s
#DefaultStartLimitBurst=5
#DefaultEnvironment=
#DefaultLimitCPU=
#DefaultLimitFSIZE=
#DefaultLimitDATA=
#DefaultLimitSTACK=
#DefaultLimitCORE=
#DefaultLimitRSS=
#DefaultLimitNOFILE=
#DefaultLimitAS=
#DefaultLimitNPROC=
#DefaultLimitMEMLOCK=
#DefaultLimitLOCKS=
#DefaultLimitSIGPENDING=
#DefaultLimitMSGQUEUE=
#DefaultLimitNICE=
#DefaultLimitRTPRIO=
#DefaultLimitRTTIME=
DefaultLimitNOFILE=1966055
EOF
  cat >> /etc/systemd/system.conf <<EOF
[Manager]
#LogLevel=info
#LogTarget=journal-or-kmsg
#LogColor=yes
#LogLocation=no
#DumpCore=yes
#ShowStatus=yes
#CrashChangeVT=no
#CrashShell=no
#CrashReboot=no
#CtrlAltDelBurstAction=reboot-force
#CPUAffinity=1 2
#NUMAPolicy=default
#NUMAMask=
#RuntimeWatchdogSec=0
#RebootWatchdogSec=10min
#ShutdownWatchdogSec=10min
#KExecWatchdogSec=0
#WatchdogDevice=
#CapabilityBoundingSet=
#NoNewPrivileges=no
#SystemCallArchitectures=
#TimerSlackNSec=
#StatusUnitFormat=description
#DefaultTimerAccuracySec=1min
#DefaultStandardOutput=journal
#DefaultStandardError=inherit
#DefaultTimeoutStartSec=90s
#DefaultTimeoutStopSec=90s
#DefaultTimeoutAbortSec=
#DefaultRestartSec=100ms
#DefaultStartLimitIntervalSec=10s
#DefaultStartLimitBurst=5
#DefaultEnvironment=
#DefaultCPUAccounting=no
#DefaultIOAccounting=no
#DefaultIPAccounting=no
#DefaultBlockIOAccounting=no
#DefaultMemoryAccounting=yes
#DefaultTasksAccounting=yes
#DefaultTasksMax=
#DefaultLimitCPU=
#DefaultLimitFSIZE=
#DefaultLimitDATA=
#DefaultLimitSTACK=
#DefaultLimitCORE=
#DefaultLimitRSS=
#DefaultLimitNOFILE=1024:524288
#DefaultLimitAS=
#DefaultLimitNPROC=
#DefaultLimitMEMLOCK=
#DefaultLimitLOCKS=
#DefaultLimitSIGPENDING=
#DefaultLimitMSGQUEUE=
#DefaultLimitNICE=
#DefaultLimitRTPRIO=
#DefaultLimitRTTIME=
DefaultLimitNOFILE=196605
EOF
}

install_nginx(){
  echo "create nginx.conf"
  cat > /etc/nginx/nginx.conf << EOF
user root;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;
worker_rlimit_nofile 131072;


events {
	  worker_connections 131072;
    use epoll;
    multi_accept on;
}

http {

    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 60;
    types_hash_max_size 2048;

    include /etc/nginx/mime.types;
    default_type application/json;


    ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
    ssl_prefer_server_ciphers on;

    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;

    gzip on;
    gzip_min_length 1k;
    gzip_buffers 4 16k;
    gzip_comp_level 2;
    gzip_types  application/json  text/plain application/javascript application/x-javascript text/css application/xml text/javascript application/x-httpd-php image/jpeg image/gif image/png application/font-woff;
    gzip_vary off;
    gzip_disable "MSIE [1-6]\.";


	include /etc/nginx/conf.d/*.conf;
	include /etc/nginx/sites-enabled/*;
}
EOF
  echo "create /etc/nginx/sites-available/default"
  cat > /etc/nginx/sites-available/default << EOF
autoindex on;
server {
        listen 800 default_server;
        listen [::]:800 default_server;
        root /var/www/html;
        server_name _;
        location / {

        }
}
EOF
  service nginx restart
  echo "restart nginx success"
}


install_node_export(){
  echo "sync time..."
  sudo apt update
  timedatectl set-timezone Asia/Shanghai  # sync the server's time as China shanghai's time
  date -R  
  hwclock --systohc
  echo "sync time finished"
  mkdir /usr/node
  pushd /usr/node
  echo "download node_exporter"
  wget https://gitlab.com/Chanli520/downloadfile/-/raw/master/node_exporter-1.0.1.linux-amd64.tar.gz
  tar zxvf  node_exporter-1.0.1.linux-amd64.tar.gz
  mv  node_exporter-1.0.1.linux-amd64  node_ep
  pushd node_ep
  echo "download success"
  # start command  ./node_exporter --collector.systemd --collector.systemd.unit-whitelist='(supervisor||nginx||node_exporter)'.service

  cat > /lib/systemd/system/node_exporter.service  <<EOF
[Unit]
Description=node_exporter
Documentation=node_exporter Monitoring System
After=network.target

[Service]
ExecStart=/usr/node/node_ep/node_exporter --collector.systemd --collector.systemd.unit-whitelist='(supervisor||nginx||node_exporter)'.service --web.listen-address=:9300
KillMode=process
Restart=on-failure
RestartSec=10s


[Install]
WantedBy=multi-user.target
EOF
chmod 766 /lib/systemd/system/node_exporter.service
systemctl enable node_exporter.service
systemctl daemon-reload
service node_exporter restart
sleep 3
service node_exporter status
}




setup_auto_supervisor(){
  supervisorctl stop all
if [ ! -f "/lib/systemd/system/supervisor.service" ]
then
   cat > /lib/systemd/system/supervisor.service  <<EOF
[Unit]
Description=supervisor
After=network.target

[Service]
Type=forking
ExecStart=/usr/local/bin/supervisord -c /etc/supervisord.conf
ExecStop=/usr/local/bin/supervisorctl \$OPTIONS shutdown
ExecReload=/usr/local/bin/supervisorctl \$OPTIONS reload
KillMode=process
Restart=on-failure
RestartSec=42s

[Install]
WantedBy=multi-user.target
EOF
  chmod 766 /lib/systemd/system/supervisor.service
  systemctl enable supervisor.service
  systemctl daemon-reload
  killall -9 supervisord
  service supervisor restart
  echo "Install supervisord services Success"
else
  echo "had installed supervisord services"
fi
}

write_new_supervisor_conf(){
   cat > /etc/supervisord.conf << EOF
[unix_http_server]
file=/tmp/supervisor.sock   ; the path to the socket file

[supervisord]
logfile=/tmp/supervisord.log ; main log file; default $CWD/supervisord.log
logfile_maxbytes=50MB        ; max main logfile bytes b4 rotation; default 50MB
logfile_backups=10           ; # of main logfile backups; 0 means none, default 10
loglevel=info                ; log level; default info; others: debug,warn,trace
pidfile=/tmp/supervisord.pid ; supervisord pidfile; default supervisord.pid
nodaemon=false               ; start in foreground if true; default false
silent=false                 ; no logs to stdout if true; default false
minfds=1024                  ; min. avail startup file descriptors; default 1024
minprocs=200                 ; min. avail process descriptors;default 200

[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

[supervisorctl]
serverurl=unix:///tmp/supervisor.sock ; use a unix:// URL  for a unix socket

[include]
files = /etc/supervisor_configs/*.conf
EOF

}


setup_supervisor(){
  if [ -f "/etc/supervisord.conf" ];then
    echo "had installed supervisor"
  else
    pip3 install supervisor==4.2.2
    ehco "install supervisor success"
fi
  if [ ! -d "/etc/supervisor_configs" ];then
    echo "create new supervisor_configs dir"
    mkdir /etc/supervisor_configs
  else
    echo "supervisor_configs dir had exists"
fi
  echo "rewrite supervisor.conf"
  write_new_supervisor_conf
  supervisord -c /etc/supervisord.conf
  setup_auto_supervisor
}


setup_auto_restart_manager(){
  cat > /etc/crontab << EOF
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

17 *    * * *   root    cd / && run-parts --report /etc/cron.hourly
25 6    * * *   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
47 6    * * 7   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
52 6    1 * *   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
30 9    * * *   root    /usr/local/bin/supervisorctl reload
EOF
  service cron restart
  echo "setup crontab task success"
}

install_new_control(){
  youhua
  install_nginx
  install_node_export
  if [ -d "/LightsailVpn" ];then
      echo "Old Version had exists...."
      delete_old_ss_services
    else
      echo "Old Version not exists"
  fi
  deploy_new_control
  setup_supervisor
  setup_auto_restart_manager
  echo "restart supervisor service"
  service supervisor restart
  sleep 3
  supervisorctl status
}

install_new_control
