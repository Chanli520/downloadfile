# ban  bt
iptables -I OUTPUT  -t  mangle   -p tcp -m ipp2p --bit -j DROP
iptables -I OUTPUT  -t  mangle   -p udp -m ipp2p --bit -j DROP

# ban edk
iptables -I OUTPUT  -t  mangle   -p udp -m ipp2p --edk -j DROP
iptables -I OUTPUT  -t  mangle   -p tcp -m ipp2p --edk -j DROP

# ban db  apple  winmx  soul  areas
iptables -I OUTPUT  -t  mangle   -p tcp -m ipp2p --dc -j DROP
iptables -I OUTPUT  -t  mangle   -p tcp -m ipp2p --apple -j DROP
iptables -I OUTPUT  -t  mangle   -p tcp -m ipp2p --winmx -j DROP
iptables -I OUTPUT  -t  mangle   -p tcp -m ipp2p --soul  -j DROP
iptables -I OUTPUT  -t  mangle   -p tcp -m ipp2p --ares  -j DROP

# ban kazaa
iptables -I OUTPUT  -t  mangle   -p udp -m ipp2p --kazaa -j DROP
iptables -I OUTPUT  -t  mangle   -p tcp -m ipp2p --kazaa -j DROP

# ban gnu
iptables -I OUTPUT  -t  mangle   -p udp -m ipp2p --gnu   -j DROP
iptables -I OUTPUT  -t  mangle   -p tcp -m ipp2p --gnu   -j DROP
