#!/bin/sh
daemon_start(){
  ssh -N -p 22 root@54.251.222.120 -L 27017:nekodb.cluster-chun4hqiskdp.ap-southeast-1.docdb.amazonaws.com:27017 &
  echo "SSh tunnel started."
}

daemon_stop(){
  ps -ef | grep "ssh -N -p 22" | grep -v grep | awk '{print $2}' | xargs kill -9
  echo "SSh tunnel Killed."
}

case "$1" in
  start)
    daemon_start
    ;;
  stop)
    daemon_stop
    ;;
  restart)
    daemon_stop
    daemon_start
    ;;
  *)
  echo "Usage: Services {start|stop|restart}"
  exit 1
esac
