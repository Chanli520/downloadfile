#!/bin/bash
shopt -s expand_aliases
Font_Black="\033[30m"
Font_Red="\033[31m"
Font_Green="\033[32m"
Font_Yellow="\033[33m"
Font_Blue="\033[34m"
Font_Purple="\033[35m"
Font_SkyBlue="\033[36m"
Font_White="\033[37m"
Font_Suffix="\033[0m"

while getopts ":I:M:EX:P:S:p:" optname; do
    case "$optname" in
    "I")
        iface="$OPTARG"
        useNIC="--interface $iface"
        ;;
    "M")
        if [[ "$OPTARG" == "4" ]]; then
            NetworkType=4
        elif [[ "$OPTARG" == "6" ]]; then
            NetworkType=6
        fi
        ;;
    "E")
        language="e"
        ;;
    "X")
        XIP="$OPTARG"
        xForward="--header X-Forwarded-For:$XIP"
        ;;
    "P")
        proxy="$OPTARG"
        usePROXY="-x $proxy"
        ;;
    "S")
        num="$OPTARG"
        ;;
    "p")
        sniProxy=${OPTARG}
        echo "proxy = ${sniProxy}"
        ;;
    ":")
        echo "Unknown error while processing options"
        exit 1
        ;;
    esac

done
shift $((OPTIND-1))

if [ -z "$iface" ]; then
    useNIC=""
fi

if [ -z "$XIP" ]; then
    xForward=""
fi

if [ -z "$proxy" ]; then
    usePROXY=""
elif [ -n "$proxy" ]; then
    NetworkType=4
fi

if ! mktemp -u --suffix=RRC &>/dev/null; then
    is_busybox=1
fi

UA_Browser="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36"
UA_Dalvik="Dalvik/2.1.0 (Linux; U; Android 9; ALP-AL00 Build/HUAWEIALP-AL00)"
#Media_Cookie=$(curl -s --retry 3 --max-time 10 "https://raw.githubusercontent.com/lmc999/RegionRestrictionCheck/main/cookies")
#IATACode=$(curl -s --retry 3 --max-time 10 "https://raw.githubusercontent.com/lmc999/RegionRestrictionCheck/main/reference/IATACode.txt")
#WOWOW_Cookie=$(echo "$Media_Cookie" | awk 'NR==3')
TVer_Cookie="Accept: application/json;pk=BCpkADawqM0_rzsjsYbC1k1wlJLU4HiAtfzjxdUmfvvLUQB-Ax6VA-p-9wOEZbCEm3u95qq2Y1CQQW1K9tPaMma9iAqUqhpISCmyXrgnlpx9soEmoVNuQpiyGsTpePGumWxSs1YoKziYB6Wz"

countRunTimes() {
    if [ "$is_busybox" == 1 ]; then
        count_file=$(mktemp)
    else
        count_file=$(mktemp --suffix=RRC)
    fi
    RunTimes=$(curl -s --max-time 10 "https://hits.seeyoufarm.com/api/count/incr/badge.svg?url=https%3A%2F%2Fcheck.unclock.media&count_bg=%2379C83D&title_bg=%23555555&icon=&icon_color=%23E7E7E7&title=visit&edge_flat=false" >"${count_file}")
    TodayRunTimes=$(cat "${count_file}" | tail -3 | head -n 1 | awk '{print $5}')
    TotalRunTimes=$(($(cat "${count_file}" | tail -3 | head -n 1 | awk '{print $7}') + 2527395))
}
countRunTimes

checkOS() {
    ifTermux=$(echo $PWD | grep termux)
    ifMacOS=$(uname -a | grep Darwin)
    if [ -n "$ifTermux" ]; then
        os_version=Termux
        is_termux=1
    elif [ -n "$ifMacOS" ]; then
        os_version=MacOS
        is_macos=1
    else
        os_version=$(grep 'VERSION_ID' /etc/os-release | cut -d '"' -f 2 | tr -d '.')
    fi

    if [[ "$os_version" == "2004" ]] || [[ "$os_version" == "10" ]] || [[ "$os_version" == "11" ]]; then
        is_windows=1
        ssll="-k --ciphers DEFAULT@SECLEVEL=1"
    fi

    if [ "$(which apt 2>/dev/null)" ]; then
        InstallMethod="apt"
        is_debian=1
    elif [ "$(which dnf 2>/dev/null)" ] || [ "$(which yum 2>/dev/null)" ]; then
        InstallMethod="yum"
        is_redhat=1
    elif [[ "$os_version" == "Termux" ]]; then
        InstallMethod="pkg"
    elif [[ "$os_version" == "MacOS" ]]; then
        InstallMethod="brew"
    fi
}
checkOS

checkCPU() {
    CPUArch=$(uname -m)
    if [[ "$CPUArch" == "aarch64" ]]; then
        arch=_arm64
    elif [[ "$CPUArch" == "i686" ]]; then
        arch=_i686
    elif [[ "$CPUArch" == "arm" ]]; then
        arch=_arm
    elif [[ "$CPUArch" == "x86_64" ]] && [ -n "$ifMacOS" ]; then
        arch=_darwin
    fi
}
checkCPU

checkDependencies() {

    # os_detail=$(cat /etc/os-release 2> /dev/null)

    if ! command -v python &>/dev/null; then
        if command -v python3 &>/dev/null; then
            alias python="python3"
        else
            if [ "$is_debian" == 1 ]; then
                echo -e "${Font_Green}Installing python${Font_Suffix}"
                $InstallMethod update >/dev/null 2>&1
                $InstallMethod install python -y >/dev/null 2>&1
            elif [ "$is_redhat" == 1 ]; then
                echo -e "${Font_Green}Installing python${Font_Suffix}"
                if [[ "$os_version" -gt 7 ]]; then
                    $InstallMethod makecache >/dev/null 2>&1
                    $InstallMethod install python3 -y >/dev/null 2>&1
                    alias python="python3"
                else
                    $InstallMethod makecache >/dev/null 2>&1
                    $InstallMethod install python -y >/dev/null 2>&1
                fi

            elif [ "$is_termux" == 1 ]; then
                echo -e "${Font_Green}Installing python${Font_Suffix}"
                $InstallMethod update -y >/dev/null 2>&1
                $InstallMethod install python -y >/dev/null 2>&1

            elif [ "$is_macos" == 1 ]; then
                echo -e "${Font_Green}Installing python${Font_Suffix}"
                $InstallMethod install python
            fi
        fi
    fi

    if ! command -v dig &>/dev/null; then
        if [ "$is_debian" == 1 ]; then
            echo -e "${Font_Green}Installing dnsutils${Font_Suffix}"
            $InstallMethod update >/dev/null 2>&1
            $InstallMethod install dnsutils -y >/dev/null 2>&1
        elif [ "$is_redhat" == 1 ]; then
            echo -e "${Font_Green}Installing bind-utils${Font_Suffix}"
            $InstallMethod makecache >/dev/null 2>&1
            $InstallMethod install bind-utils -y >/dev/null 2>&1
        elif [ "$is_termux" == 1 ]; then
            echo -e "${Font_Green}Installing dnsutils${Font_Suffix}"
            $InstallMethod update -y >/dev/null 2>&1
            $InstallMethod install dnsutils -y >/dev/null 2>&1
        elif [ "$is_macos" == 1 ]; then
            echo -e "${Font_Green}Installing bind${Font_Suffix}"
            $InstallMethod install bind
        fi
    fi

#    if [ "$is_macos" == 1 ]; then
#        if ! command -v md5sum &>/dev/null; then
#            echo -e "${Font_Green}Installing md5sha1sum${Font_Suffix}"
#            $InstallMethod install md5sha1sum
#        fi
#    fi

}
checkDependencies

#188.165.56.21 检测出来fr, 但是实际上用不了
function MediaUnlockTest_Netflix() {
    local result1=$(curl $useNIC $usePROXY $xForward -${1} --user-agent "${UA_Browser}" -fsL --write-out %{http_code} --output /dev/null --max-time 5 "https://www.netflix.com/title/81280792" 2>&1 )
    local result2=$(curl $useNIC $usePROXY $xForward -${1} --user-agent "${UA_Browser}" -fsL --write-out %{http_code} --output /dev/null --max-time 5 "https://www.netflix.com/title/70143836" 2>&1 )
    
    if [[ "$result1" == "404" ]] && [[ "$result2" == "404" ]]; then
        echo -n -e "\r Netflix:\t\t\t\t${Font_Yellow}Originals Only${Font_Suffix}\n"
        return
    elif [[ "$result1" == "403" ]] && [[ "$result2" == "403" ]]; then
        echo -n -e "\r Netflix:\t\t\t\t${Font_Red}No${Font_Suffix}\n"
        return
    elif [[ "$result1" == "200" && "$result2" != "000" ]] || [[ "$result2" == "200" && "$result1" != "000" ]]; then
        local cookie=$(echo 'cookie: SecureNetflixId=v%3D2%26mac%3DAQEAEQABABRPVYntWcDE7gA4-7SvVpINvicduBVqLFk.%26dt%3D1697907371888; NetflixId=ct%3DBQAOAAEBEIS456b5fMFtLDUF98BXgy-CUJmC4p5TerrOY_CezP7zdtsHvvOX_D3K57RbSvga35JYIxWOZDfp0GUcfTTwk_xD-wZPHTUYZuvVzM49pVi4H7DflG5eeNEzNs8zm2PsT6fo4WxPb94rPy5z8sAWnvE09DfSc5lTEGL23REYkW7rzhkOycXdSM26x0L41sff6ARrxTdd9QvI6rNw0seSn-Wvi-rIC2wvgNXWCvciahZWF199C0ESTCltSsMRMbfg13xP5lid8MVVmeDSObApcdoD5BD29_t9QLvLyuAcc_kdkXdXCbyPbzu04RM_qTdh9NYli3UVgLkJtA3j8NlL8WOMVX-h8nrbfVvqnV1XwzL-_VeShwaqtvL8Y8I2kmQzc1-AVinz_DYPtwXcl05kwhBEq3FBoB728RgbHCTm85JE40OoYEpY6oQnflwES9HsAgVto20ylV9SgWJjwKsU7Kp-duICZHW9bLoSCO_2u2F7_JSxZCtZ9j8s1HJ9B_sD4ndruDaN7oRmOGckGp_gwyJDechEBEpzBUKum56vw5NyG9eRss2THFhrGObsLO_p_VnETLWaFM7QCrzKkVn_VubzvRAXakaOFVOtbsJTWAMuqXs4FW3HNt7YVKhwL8XQcJd0GZY9zE19keBqL2zIKLbVdaQz7iiTHRkQhnrw4R7Ft9-HCuXTGiqGW_8-E_UFUjHwzN0GzPcfjs4xFWF8GUpDX5IYSOlAr33SMhJQvPrxjyyTFKFvj7MyrSTFSLliUiUtWuRZIxNtfV74bvrLywfwQQIIstqfGNGVrth9iNM2Gfs.%26bt%3Ddbl%26ch%3DAQEAEAABABT47tefmpZEZBcoklOASjiqFS8S8XGf1w4.%26v%3D2%26mac%3DAQEAEAABABQsKVrsUEPVHWoT4fsmgfoL3OIuEt5DKz4.; NetflixId=ct%3DBQAOAAEBEOL4PyFrqgdwhTao8WcrDBeB4ELd2UGHmiwvNOXM1QElidtveTtLO4n8sbxAw5nMSoqe3VusIQJ0LAOgMLMUpHjLkT6tLXqF2btALNMPhB9m1b9pR60m3VIZKeTJEw2VWHFXm7VXuyB5YrVnYSzN3jALdFiDAt1vKHsecc4EKw9khVat7KU07UAVfkLtDYSKqDbLdWcIi6cMhxUBarRSMsx8AYop5zEk19JKPoSANG1MLfMqEH5dU_7P_k4fHmuBSv-oMMUyjGHHzQYwbntPSLj76lMFHnaN_7fOfKdl1--uD0jAFOMN0faFDAsBkdIVSGQczotoFy4gEEwDVQmMuos6yrP4CEpGetGtWaEv55a5pOKSLF7aJtbWiEDITI0ip71ch_ccEgvz0oM-H1vB2dUUNn3clqltWr5zVUOBKBrkm_Qj49RuaEoF0HdtJQzNDTd2aoxrcHQgnTG8ce32iLIGWJvKheo_Saekax-Ylk41tQB8Uv8gXxzUJDByPCq9sSZOv70QudvtwIL_h1m62Fdk1CeD2cFLxpVBPSrOEt8PxziNg_HsEcEOIDpMhQdBta9NxhkfMBEGpZOj7ECvQwbbAqTCUReZr5vEUY-CZmeTxMjMMiqJBzU3W1CDLOHQ8fFDhfBhP8MvjVgkqjYiRagvxQ..%26bt%3Ddbl%26ch%3DAQEAEAABABT47tefmpZEZBcoklOASjiqFS8S8XGf1w4.%26v%3D2%26mac%3DAQEAEAABABSjK24hgSubg9Jt2rx6VNiSOgCz2wUBzTs.; SecureNetflixId=v%3D2%26mac%3DAQEAEQABABR4DIUIRRMY9HM3-unJfBEYkor2UPYRkRw.%26dt%3D1698026651480; nfvdid=BQFmAAEBEPNaIa_bzhq-AfFcoFG1cjxgntPGZ1BQ84dhrh5ybe8eMcKZvKQ0OcuIKs8dPo4FNeMLjond2KZ1s1Ol3hyI_GStoAI6byptZBd-utkN7dhD2UJ_5gqLt8SMM17KXWx97knJwLI_x9a-4Z5o6C_4nU6u')
        local data=$(echo '{"version":2,"url":"manifest","id":169804703120760300,"languages":["zh-CO"],"params":{"type":"standard","manifestVersion":"v2","viewableId":81167709,"profiles":["heaac-2-dash","heaac-2hq-dash","playready-h264mpl30-dash","playready-h264mpl31-dash","playready-h264hpl30-dash","playready-h264hpl31-dash","vp9-profile0-L30-dash-cenc","vp9-profile0-L31-dash-cenc","av1-main-L30-dash-cbcs-prk","av1-main-L31-dash-cbcs-prk","playready-h264mpl40-dash","playready-h264hpl40-dash","vp9-profile0-L40-dash-cenc","av1-main-L40-dash-cbcs-prk","av1-main-L41-dash-cbcs-prk","h264hpl30-dash-playready-live","h264hpl31-dash-playready-live","h264hpl40-dash-playready-live","imsc1.1","dfxp-ls-sdh","simplesdh","nflx-cmisc","BIF240","BIF320"],"flavor":"SUPPLEMENTAL","drmType":"widevine","drmVersion":25,"usePsshBox":true,"isBranching":false,"useHttpsStreams":true,"supportsUnequalizedDownloadables":true,"imageSubtitleHeight":720,"uiVersion":"shakti-vc2bfd55a","uiPlatform":"SHAKTI","clientVersion":"6.0042.521.911","platform":"118.0.0.0","osVersion":"10.15.7","osName":"mac","supportsPreReleasePin":true,"supportsWatermark":true,"videoOutputInfo":[{"type":"DigitalVideoOutputDescriptor","outputType":"unknown","supportedHdcpVersions":[],"isHdcpEngaged":false}],"titleSpecificData":{"81167709":{"unletterboxed":true}},"preferAssistiveAudio":false,"isUIAutoPlay":true,"isNonMember":false,"desiredVmaf":"plus_lts","desiredSegmentVmaf":"plus_lts","requestSegmentVmaf":false,"supportsPartialHydration":true,"contentPlaygraph":["start"],"supportsAdBreakHydration":true,"liveMetadataFormat":"INDEXED_SEGMENT_TEMPLATE","useBetterTextUrls":true,"maxSupportedLanguages":2}}')
        local result0=$(curl -fsL --location 'https://www.netflix.com/nq/website/memberapi/vc2bfd55a/pathEvaluator?original_path=%2Fshakti%2Fmre%2FpathEvaluator' --header 'accept: */*' --header 'cache-control: no-cache' --header 'content-type: application/x-www-form-urlencoded' --header "$cookie" --data-urlencode 'path=["videos",70143836,["availability","inRemindMeList","queue"]]' --max-time 5 | tr -d '[:space:]' | grep '"isPlayable":false')
        if [[ -n "$result0" ]]; then
            echo -n -e "\r Netflix:\t\t\t\t${Font_Red}Failed (Not Playable)${Font_Suffix}\n"
            return
        fi
        local result_list=$(curl -fsL --location 'https://www.netflix.com/playapi/cadmium/manifest/1?reqAttempt=1&reqName=manifest&clienttype=akira&uiversion=vc2bfd55a&browsername=chrome&browserversion=118.0.0.0&osname=mac&osversion=10.15.7' --header 'accept: */*' --header 'cache-control: no-cache' --header 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36' --header 'Content-Type: text/plain' --header "$cookie" --data "$data" --max-time 5)
        local url=$(echo "$result_list" | grep -o 'https://[^"]*' | grep -m 1 'https://[^"]*')
        if [[ -n "$url" ]]; then
            local domain=$(echo "$url" | cut -d'/' -f3)
            local video_http_code=$(curl "$url" --user-agent "${UA_Browser}" -fsL --write-out %{http_code} --output /dev/null --max-time 5 2>&1 )
#            echo -n -e "Netflix: $video_http_code\n"
#            return
            if [[ "$video_http_code" != "200" ]] && [[ "$video_http_code" != "403" ]]; then
                echo -n -e "\r Netflix:\t\t\t\t${Font_Red}Failed (Video Failed:$video_http_code)${Font_Suffix}\n"
                return
            fi
        fi
        local region=$(curl $useNIC $usePROXY $xForward -${1} --user-agent "${UA_Browser}" -fs --max-time 5 --write-out %{redirect_url} --output /dev/null "https://www.netflix.com/title/80018499" 2>&1 | cut -d '/' -f4 | cut -d '-' -f1 | tr [:lower:] [:upper:])
#        if [[ ! -n "$region" ]]; then
#            local region2=$(curl --max-time 5 -sL "https://api.country.is/$sniProxy" | tr -d '[:space:]' | grep -o '"country":"[^"]*' | cut -d':' -f2 | tr -d '"' 2>&1 )
#            region="$region2"
#        fi
        if [[ ! -n "$region" ]]; then
            region="US"
        fi
        echo -n -e "\r Netflix:\t\t\t\t${Font_Green}Yes (Region: ${region})${Font_Suffix}\n"
        return
    elif [[ "$result1" == "000" ]]; then
        echo -n -e "\r Netflix:\t\t\t\t${Font_Red}Failed (Network Connection)${Font_Suffix}\n"
        return
    fi
}

function Global_UnlockTest() {
    echo ""
    echo "============[ Multination ]============"
    local result=$(
#    MediaUnlockTest_Dazn ${1} &
#    MediaUnlockTest_HotStar ${1} &
#    MediaUnlockTest_DisneyPlus ${1} &
    MediaUnlockTest_Netflix ${1} &
#    MediaUnlockTest_YouTube_Premium ${1} &
#    MediaUnlockTest_PrimeVideo_Region ${1} &
#    MediaUnlockTest_TVBAnywhere ${1} &
#    MediaUnlockTest_iQYI_Region ${1} &
#    MediaUnlockTest_Viu.com ${1} &
#    MediaUnlockTest_YouTube_CDN ${1} &
#    MediaUnlockTest_NetflixCDN ${1} &
#    MediaUnlockTest_Spotify ${1} &
#    OpenAITest ${1} &
    #MediaUnlockTest_Instagram.Music ${1} &
#    GameTest_Steam ${1} &
    )
    wait
#    local array=("Dazn:" "HotStar:" "Disney+:" "Netflix:" "YouTube Premium:" "Amazon Prime Video:" "TVBAnywhere+:" "iQyi Oversea Region:" "Viu.com:" "YouTube CDN:" "YouTube Region:" "Netflix Preferred CDN:" "Spotify Registration:" "Steam Currency:" "ChatGPT:")
    local array=("Netflix:")
    echo_Result ${result} ${array}
    echo "======================================="
}

function echo_Result() {
    for((i=0;i<${#array[@]};i++))
    do
        echo "$result" | grep "${array[i]}"
        sleep 0.03
    done;
}

# 测试disney+ and netflix的解锁能力
Global_UnlockTest 4
