

check_is_installed(){
  cat /etc/iptables-script | grep "edk" | wc -l
}


install_ipp2p(){

  get_kernel_version=`uname -r`
  echo "Linux kernel version is: $get_kernel_version"
  if [[ ${get_kernel_version:0:3}  =~ "5.5" ]]
then
      echo "This Kernel is the Latest Version,Not install ipp2p"
else
      # delete the old keywords
      nums=$(iptables -t mangle -L OUTPUT -nvx --line-numbers|grep "DROP" | awk '{print $1}'  |wc -l)

      for((integer=1; integer<=${nums}; integer++))
      do
          iptables -t mangle -D OUTPUT 1
          echo "delete ${integer} times"
      done

      sleep 1

      echo "show after dele iptabels "
      iptables -L -v -t mangle

      # install ipp2p
      apt-get update
      apt-get install -y xtables-addons-common

      echo "show after p2p iptabels "
      iptables -L -v -t mangle

      echo "iptables-save > /etc/iptables-script"

      cat > /etc/iptables-script <<EOF
# Generated by iptables-save v1.6.1 on Tue Mar 10 10:33:10 2020
*mangle
:PREROUTING ACCEPT [3511418015:4379780840542]
:INPUT ACCEPT [3511417900:4379780807896]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [3054717125:3704854539368]
:POSTROUTING ACCEPT [3054709941:3704855005259]
-A OUTPUT -p udp -m ipp2p --edk  --gnu  --kazaa  --bit  -j DROP
-A OUTPUT -p tcp -m ipp2p --edk  --dc  --gnu  --kazaa  --bit  --apple  --soul  --winmx  --ares  -j DROP
-A OUTPUT -m string --string "torrent" --algo bm --to 65535 -j DROP
-A OUTPUT -m string --string ".torrent" --algo bm --to 65535 -j DROP
-A OUTPUT -m string --string "peer_id=" --algo bm --to 65535 -j DROP
-A OUTPUT -m string --string "announce" --algo bm --to 65535 -j DROP
-A OUTPUT -m string --string "info_hash" --algo bm --to 65535 -j DROP
-A OUTPUT -m string --string "get_peers" --algo bm --to 65535 -j DROP
-A OUTPUT -m string --string "find_node" --algo bm --to 65535 -j DROP
-A OUTPUT -m string --string "BitTorrent" --algo bm --to 65535 -j DROP
-A OUTPUT -m string --string "announce_peer" --algo bm --to 65535 -j DROP
-A OUTPUT -m string --string "BitTorrent protocol" --algo bm --to 65535 -j DROP
-A OUTPUT -m string --string "announce.php?passkey=" --algo bm --to 65535 -j DROP
-A OUTPUT -m string --string "magnet:" --algo bm --to 65535 -j DROP
-A OUTPUT -m string --string "xunlei" --algo bm --to 65535 -j DROP
-A OUTPUT -m string --string "sandai" --algo bm --to 65535 -j DROP
-A OUTPUT -m string --string "Thunder" --algo bm --to 65535 -j DROP
-A OUTPUT -m string --string "XLLiveUD" --algo bm --to 65535 -j DROP
COMMIT
# Completed on Tue Mar 10 10:33:10 2020
# Generated by iptables-save v1.6.1 on Tue Mar 10 10:33:10 2020
*filter
:INPUT ACCEPT [269087225:302987416466]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [236915610:253210938401]
-A OUTPUT -p tcp -m multiport --dports 25,26,465,587 -m state --state NEW,ESTABLISHED -j REJECT --reject-with icmp-port-unreachable
-A OUTPUT -p udp -m multiport --dports 25,26,465,587 -j DROP
COMMIT
# Completed on Tue Mar 10 10:33:10 2020
EOF

      echo "show the script"
      cat /etc/iptables-script

      echo "restart the ss_2 service"
      service ss_2 restart
      sleep 3
      echo "service ss_2 status"
      service ss_2 status

      echo "show after p2p iptabels "
      iptables -L -v -t mangle
fi


}


is_installed=`check_is_installed`
if [[ ${is_installed} == "0" ]]
then
    install_ipp2p
else
  echo "had install_ipp2p finished"
fi




