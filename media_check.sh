#!/bin/bash
shopt -s expand_aliases
Font_Black="\033[30m"
Font_Red="\033[31m"
Font_Green="\033[32m"
Font_Yellow="\033[33m"
Font_Blue="\033[34m"
Font_Purple="\033[35m"
Font_SkyBlue="\033[36m"
Font_White="\033[37m"
Font_Suffix="\033[0m"

while getopts ":I:M:EX:P:S:p:" optname; do
    case "$optname" in
    "I")
        iface="$OPTARG"
        useNIC="--interface $iface"
        ;;
    "M")
        if [[ "$OPTARG" == "4" ]]; then
            NetworkType=4
        elif [[ "$OPTARG" == "6" ]]; then
            NetworkType=6
        fi
        ;;
    "E")
        language="e"
        ;;
    "X")
        XIP="$OPTARG"
        xForward="--header X-Forwarded-For:$XIP"
        ;;
    "P")
        proxy="$OPTARG"
        usePROXY="-x $proxy"
        ;;
    "S")
        num="$OPTARG"
        ;;
    "p")
        sniProxy=${OPTARG}
        echo "proxy = ${sniProxy}"
        ;;
    ":")
        echo "Unknown error while processing options"
        exit 1
        ;;
    esac

done
shift $((OPTIND-1))

if [ -z "$iface" ]; then
    useNIC=""
fi

if [ -z "$XIP" ]; then
    xForward=""
fi

if [ -z "$proxy" ]; then
    usePROXY=""
elif [ -n "$proxy" ]; then
    NetworkType=4
fi

if ! mktemp -u --suffix=RRC &>/dev/null; then
    is_busybox=1
fi

UA_Browser="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36"
UA_Dalvik="Dalvik/2.1.0 (Linux; U; Android 9; ALP-AL00 Build/HUAWEIALP-AL00)"
Media_Cookie=$(curl -s --retry 3 --max-time 6 "https://raw.githubusercontent.com/lmc999/RegionRestrictionCheck/main/cookies")
IATACode=$(curl -s --retry 3 --max-time 6 "https://raw.githubusercontent.com/lmc999/RegionRestrictionCheck/main/reference/IATACode.txt")
WOWOW_Cookie=$(echo "$Media_Cookie" | awk 'NR==3')
TVer_Cookie="Accept: application/json;pk=BCpkADawqM0_rzsjsYbC1k1wlJLU4HiAtfzjxdUmfvvLUQB-Ax6VA-p-9wOEZbCEm3u95qq2Y1CQQW1K9tPaMma9iAqUqhpISCmyXrgnlpx9soEmoVNuQpiyGsTpePGumWxSs1YoKziYB6Wz"

countRunTimes() {
    if [ "$is_busybox" == 1 ]; then
        count_file=$(mktemp)
    else
        count_file=$(mktemp --suffix=RRC)
    fi
    RunTimes=$(curl -s --max-time 6 "https://hits.seeyoufarm.com/api/count/incr/badge.svg?url=https%3A%2F%2Fcheck.unclock.media&count_bg=%2379C83D&title_bg=%23555555&icon=&icon_color=%23E7E7E7&title=visit&edge_flat=false" >"${count_file}")
    TodayRunTimes=$(cat "${count_file}" | tail -3 | head -n 1 | awk '{print $5}')
    TotalRunTimes=$(($(cat "${count_file}" | tail -3 | head -n 1 | awk '{print $7}') + 2527395))
}
countRunTimes

checkOS() {
    ifTermux=$(echo $PWD | grep termux)
    ifMacOS=$(uname -a | grep Darwin)
    if [ -n "$ifTermux" ]; then
        os_version=Termux
        is_termux=1
    elif [ -n "$ifMacOS" ]; then
        os_version=MacOS
        is_macos=1
    else
        os_version=$(grep 'VERSION_ID' /etc/os-release | cut -d '"' -f 2 | tr -d '.')
    fi

    if [[ "$os_version" == "2004" ]] || [[ "$os_version" == "10" ]] || [[ "$os_version" == "11" ]]; then
        is_windows=1
        ssll="-k --ciphers DEFAULT@SECLEVEL=1"
    fi

    if [ "$(which apt 2>/dev/null)" ]; then
        InstallMethod="apt"
        is_debian=1
    elif [ "$(which dnf 2>/dev/null)" ] || [ "$(which yum 2>/dev/null)" ]; then
        InstallMethod="yum"
        is_redhat=1
    elif [[ "$os_version" == "Termux" ]]; then
        InstallMethod="pkg"
    elif [[ "$os_version" == "MacOS" ]]; then
        InstallMethod="brew"
    fi
}
checkOS

checkCPU() {
    CPUArch=$(uname -m)
    if [[ "$CPUArch" == "aarch64" ]]; then
        arch=_arm64
    elif [[ "$CPUArch" == "i686" ]]; then
        arch=_i686
    elif [[ "$CPUArch" == "arm" ]]; then
        arch=_arm
    elif [[ "$CPUArch" == "x86_64" ]] && [ -n "$ifMacOS" ]; then
        arch=_darwin
    fi
}
checkCPU

checkDependencies() {

    # os_detail=$(cat /etc/os-release 2> /dev/null)

    if ! command -v python &>/dev/null; then
        if command -v python3 &>/dev/null; then
            alias python="python3"
        else
            if [ "$is_debian" == 1 ]; then
                echo -e "${Font_Green}Installing python${Font_Suffix}"
                $InstallMethod update >/dev/null 2>&1
                $InstallMethod install python -y >/dev/null 2>&1
            elif [ "$is_redhat" == 1 ]; then
                echo -e "${Font_Green}Installing python${Font_Suffix}"
                if [[ "$os_version" -gt 7 ]]; then
                    $InstallMethod makecache >/dev/null 2>&1
                    $InstallMethod install python3 -y >/dev/null 2>&1
                    alias python="python3"
                else
                    $InstallMethod makecache >/dev/null 2>&1
                    $InstallMethod install python -y >/dev/null 2>&1
                fi

            elif [ "$is_termux" == 1 ]; then
                echo -e "${Font_Green}Installing python${Font_Suffix}"
                $InstallMethod update -y >/dev/null 2>&1
                $InstallMethod install python -y >/dev/null 2>&1

            elif [ "$is_macos" == 1 ]; then
                echo -e "${Font_Green}Installing python${Font_Suffix}"
                $InstallMethod install python
            fi
        fi
    fi

    if ! command -v dig &>/dev/null; then
        if [ "$is_debian" == 1 ]; then
            echo -e "${Font_Green}Installing dnsutils${Font_Suffix}"
            $InstallMethod update >/dev/null 2>&1
            $InstallMethod install dnsutils -y >/dev/null 2>&1
        elif [ "$is_redhat" == 1 ]; then
            echo -e "${Font_Green}Installing bind-utils${Font_Suffix}"
            $InstallMethod makecache >/dev/null 2>&1
            $InstallMethod install bind-utils -y >/dev/null 2>&1
        elif [ "$is_termux" == 1 ]; then
            echo -e "${Font_Green}Installing dnsutils${Font_Suffix}"
            $InstallMethod update -y >/dev/null 2>&1
            $InstallMethod install dnsutils -y >/dev/null 2>&1
        elif [ "$is_macos" == 1 ]; then
            echo -e "${Font_Green}Installing bind${Font_Suffix}"
            $InstallMethod install bind
        fi
    fi
}
checkDependencies

function MediaUnlockTest_DisneyPlus() {
    local PreAssertion=$(curl $useNIC $usePROXY $xForward -${1} --user-agent "${UA_Browser}" -s --max-time 6 -X POST "https://disney.api.edge.bamgrid.com/devices" --connect-to disney.api.edge.bamgrid.com:443:$sniProxy:443 -H "authorization: Bearer ZGlzbmV5JmJyb3dzZXImMS4wLjA.Cu56AgSfBTDag5NiRA81oLHkDZfu5L3CKadnefEAY84" -H "content-type: application/json; charset=UTF-8" -d '{"deviceFamily":"browser","applicationRuntime":"chrome","deviceProfile":"windows","attributes":{}}' 2>&1)
    if [[ "$PreAssertion" == "curl"* ]] && [[ "$1" == "6" ]]; then
        echo -n -e "\r Disney+:\t\t\t\tIPv6 Not Support\n"
        return
    elif [[ "$PreAssertion" == "curl"* ]]; then
        echo -n -e "\r Disney+:\t\t\t\tFailed (Network Connection)\n"
        return
    fi

    local assertion=$(echo $PreAssertion | python -m json.tool 2>/dev/null | grep assertion | cut -f4 -d'"')
    local PreDisneyCookie=$(echo "$Media_Cookie" | sed -n '1p')
    local disneycookie=$(echo $PreDisneyCookie | sed "s/DISNEYASSERTION/${assertion}/g")
    local TokenContent=$(curl $useNIC $usePROXY $xForward -${1} --user-agent "${UA_Browser}" -s --max-time 6 -X POST "https://disney.api.edge.bamgrid.com/token" --connect-to disney.api.edge.bamgrid.com:443:$sniProxy:443 -H "authorization: Bearer ZGlzbmV5JmJyb3dzZXImMS4wLjA.Cu56AgSfBTDag5NiRA81oLHkDZfu5L3CKadnefEAY84" -d "$disneycookie" 2>&1)
    local isBanned=$(echo $TokenContent | python -m json.tool 2>/dev/null | grep 'forbidden-location')
    local is403=$(echo $TokenContent | grep '403 ERROR')

    if [ -n "$isBanned" ] || [ -n "$is403" ]; then
        echo -n -e "\r Disney+:\t\t\t\tNo\n"
        return
    fi

    local fakecontent=$(echo "$Media_Cookie" | sed -n '8p')
    local refreshToken=$(echo $TokenContent | python -m json.tool 2>/dev/null | grep 'refresh_token' | awk '{print $2}' | cut -f2 -d'"')
    local disneycontent=$(echo $fakecontent | sed "s/ILOVEDISNEY/${refreshToken}/g")
    local tmpresult=$(curl $useNIC $usePROXY $xForward -${1} --user-agent "${UA_Browser}" -X POST -sSL --max-time 6 "https://disney.api.edge.bamgrid.com/graph/v1/device/graphql" --connect-to disney.api.edge.bamgrid.com:443:$sniProxy:443 -H "authorization: ZGlzbmV5JmJyb3dzZXImMS4wLjA.Cu56AgSfBTDag5NiRA81oLHkDZfu5L3CKadnefEAY84" -d "$disneycontent" 2>&1)
    local previewcheck=$(curl $useNIC $usePROXY $xForward -${1} -s -o /dev/null -L --max-time 6 -w '%{url_effective}\n' "https://disneyplus.com" --connect-to disneyplus.com:443:$sniProxy:443 | grep preview)
    local isUnabailable=$(echo $previewcheck | grep 'unavailable')
    
    local myRegion=$(curl  -X GET -sSL --max-time 6 "https://www.disneyplus.com" --connect-to www.disneyplus.com:443:$sniProxy:443 | grep 'Region: ' | cut -c 9-11)
        
    local region=$(echo $tmpresult | python -m json.tool 2>/dev/null | grep 'countryCode' | cut -f4 -d'"')
        
    #如果新的获取region的方法获取到, 则不会使用旧的
    if [[ -n "$myRegion" ]]; then
#    printf "%s" "my test region=${myRegion}"
    region="$myRegion"
    fi
  
    local inSupportedLocation=$(echo $tmpresult | python -m json.tool 2>/dev/null | grep 'inSupportedLocation' | awk '{print $2}' | cut -f1 -d',')
   
    if [[ "$region" == "JP" ]]; then
        echo -n -e "\r Disney+:\t\t\t\tYes (Region: [\"JP\"])\n"
        return
    elif [ -n "$region" ] && [[ "$inSupportedLocation" == "false" ]] && [ -z "$isUnabailable" ]; then
        echo -n -e "\r Disney+:\t\t\t\tAvailable For [Disney+ $region] Soon\n"
        return
    elif [ -n "$region" ] && [ -n "$isUnavailable" ]; then
        echo -n -e "\r Disney+:\t\t\t\tNo\n"
        return
    elif [ -n "$region" ] && [[ "$inSupportedLocation" == "true" ]]; then
        echo -n -e "\r Disney+:\t\t\t\tYes (Region: [\"$region\"])\n"
        return
    elif [ -z "$region" ]; then
        echo -n -e "\r Disney+:\t\t\t\tNo\n"
        return
    else
        echo -n -e "\r Disney+:\t\t\t\tFailed\n"
        return
    fi

}

function MediaUnlockTest_Netflix() {
    local result1=$(curl $useNIC $usePROXY $xForward -${1} --user-agent "${UA_Browser}" -fsL --write-out %{http_code} --output /dev/null --max-time 5 "https://www.netflix.com/title/81280792" --connect-to www.netflix.com:443:$sniProxy:443 2>&1 )
    local result2=$(curl $useNIC $usePROXY $xForward -${1} --user-agent "${UA_Browser}" -fsL --write-out %{http_code} --output /dev/null --max-time 5 "https://www.netflix.com/title/70143836" --connect-to www.netflix.com:443:$sniProxy:443 2>&1 )
    
    if [[ "$result1" == "404" ]] && [[ "$result2" == "404" ]]; then
        local region=$(curl $useNIC $usePROXY $xForward -${1} --user-agent "${UA_Browser}" -fs --max-time 5 --write-out %{redirect_url} --output /dev/null "https://www.netflix.com/title/80018499" --connect-to www.netflix.com:443:$sniProxy:443 2>&1 | cut -d '/' -f4 | cut -d '-' -f1 | tr [:lower:] [:upper:])
        echo -n -e "\r Netflix Originals:\t\t\t\tYes (Region: [\"${region}\"])\n"
        return
    elif [[ "$result1" == "403" ]] && [[ "$result2" == "403" ]]; then
        echo -n -e "\r Netflix:\t\t\t\tNo\n"
        return
    elif [[ "$result1" == "200" && "$result2" != "000" ]] || [[ "$result2" == "200" && "$result1" != "000" ]]; then
        local cookie=$(echo 'cookie: SecureNetflixId=v%3D2%26mac%3DAQEAEQABABRPVYntWcDE7gA4-7SvVpINvicduBVqLFk.%26dt%3D1697907371888; NetflixId=ct%3DBQAOAAEBEIS456b5fMFtLDUF98BXgy-CUJmC4p5TerrOY_CezP7zdtsHvvOX_D3K57RbSvga35JYIxWOZDfp0GUcfTTwk_xD-wZPHTUYZuvVzM49pVi4H7DflG5eeNEzNs8zm2PsT6fo4WxPb94rPy5z8sAWnvE09DfSc5lTEGL23REYkW7rzhkOycXdSM26x0L41sff6ARrxTdd9QvI6rNw0seSn-Wvi-rIC2wvgNXWCvciahZWF199C0ESTCltSsMRMbfg13xP5lid8MVVmeDSObApcdoD5BD29_t9QLvLyuAcc_kdkXdXCbyPbzu04RM_qTdh9NYli3UVgLkJtA3j8NlL8WOMVX-h8nrbfVvqnV1XwzL-_VeShwaqtvL8Y8I2kmQzc1-AVinz_DYPtwXcl05kwhBEq3FBoB728RgbHCTm85JE40OoYEpY6oQnflwES9HsAgVto20ylV9SgWJjwKsU7Kp-duICZHW9bLoSCO_2u2F7_JSxZCtZ9j8s1HJ9B_sD4ndruDaN7oRmOGckGp_gwyJDechEBEpzBUKum56vw5NyG9eRss2THFhrGObsLO_p_VnETLWaFM7QCrzKkVn_VubzvRAXakaOFVOtbsJTWAMuqXs4FW3HNt7YVKhwL8XQcJd0GZY9zE19keBqL2zIKLbVdaQz7iiTHRkQhnrw4R7Ft9-HCuXTGiqGW_8-E_UFUjHwzN0GzPcfjs4xFWF8GUpDX5IYSOlAr33SMhJQvPrxjyyTFKFvj7MyrSTFSLliUiUtWuRZIxNtfV74bvrLywfwQQIIstqfGNGVrth9iNM2Gfs.%26bt%3Ddbl%26ch%3DAQEAEAABABT47tefmpZEZBcoklOASjiqFS8S8XGf1w4.%26v%3D2%26mac%3DAQEAEAABABQsKVrsUEPVHWoT4fsmgfoL3OIuEt5DKz4.; NetflixId=ct%3DBQAOAAEBEOL4PyFrqgdwhTao8WcrDBeB4ELd2UGHmiwvNOXM1QElidtveTtLO4n8sbxAw5nMSoqe3VusIQJ0LAOgMLMUpHjLkT6tLXqF2btALNMPhB9m1b9pR60m3VIZKeTJEw2VWHFXm7VXuyB5YrVnYSzN3jALdFiDAt1vKHsecc4EKw9khVat7KU07UAVfkLtDYSKqDbLdWcIi6cMhxUBarRSMsx8AYop5zEk19JKPoSANG1MLfMqEH5dU_7P_k4fHmuBSv-oMMUyjGHHzQYwbntPSLj76lMFHnaN_7fOfKdl1--uD0jAFOMN0faFDAsBkdIVSGQczotoFy4gEEwDVQmMuos6yrP4CEpGetGtWaEv55a5pOKSLF7aJtbWiEDITI0ip71ch_ccEgvz0oM-H1vB2dUUNn3clqltWr5zVUOBKBrkm_Qj49RuaEoF0HdtJQzNDTd2aoxrcHQgnTG8ce32iLIGWJvKheo_Saekax-Ylk41tQB8Uv8gXxzUJDByPCq9sSZOv70QudvtwIL_h1m62Fdk1CeD2cFLxpVBPSrOEt8PxziNg_HsEcEOIDpMhQdBta9NxhkfMBEGpZOj7ECvQwbbAqTCUReZr5vEUY-CZmeTxMjMMiqJBzU3W1CDLOHQ8fFDhfBhP8MvjVgkqjYiRagvxQ..%26bt%3Ddbl%26ch%3DAQEAEAABABT47tefmpZEZBcoklOASjiqFS8S8XGf1w4.%26v%3D2%26mac%3DAQEAEAABABSjK24hgSubg9Jt2rx6VNiSOgCz2wUBzTs.; SecureNetflixId=v%3D2%26mac%3DAQEAEQABABR4DIUIRRMY9HM3-unJfBEYkor2UPYRkRw.%26dt%3D1698026651480; nfvdid=BQFmAAEBEPNaIa_bzhq-AfFcoFG1cjxgntPGZ1BQ84dhrh5ybe8eMcKZvKQ0OcuIKs8dPo4FNeMLjond2KZ1s1Ol3hyI_GStoAI6byptZBd-utkN7dhD2UJ_5gqLt8SMM17KXWx97knJwLI_x9a-4Z5o6C_4nU6u')
        local data=$(echo '{"version":2,"url":"manifest","id":169804703120760300,"languages":["zh-CO"],"params":{"type":"standard","manifestVersion":"v2","viewableId":81167709,"profiles":["heaac-2-dash","heaac-2hq-dash","playready-h264mpl30-dash","playready-h264mpl31-dash","playready-h264hpl30-dash","playready-h264hpl31-dash","vp9-profile0-L30-dash-cenc","vp9-profile0-L31-dash-cenc","av1-main-L30-dash-cbcs-prk","av1-main-L31-dash-cbcs-prk","playready-h264mpl40-dash","playready-h264hpl40-dash","vp9-profile0-L40-dash-cenc","av1-main-L40-dash-cbcs-prk","av1-main-L41-dash-cbcs-prk","h264hpl30-dash-playready-live","h264hpl31-dash-playready-live","h264hpl40-dash-playready-live","imsc1.1","dfxp-ls-sdh","simplesdh","nflx-cmisc","BIF240","BIF320"],"flavor":"SUPPLEMENTAL","drmType":"widevine","drmVersion":25,"usePsshBox":true,"isBranching":false,"useHttpsStreams":true,"supportsUnequalizedDownloadables":true,"imageSubtitleHeight":720,"uiVersion":"shakti-vc2bfd55a","uiPlatform":"SHAKTI","clientVersion":"6.0042.521.911","platform":"118.0.0.0","osVersion":"10.15.7","osName":"mac","supportsPreReleasePin":true,"supportsWatermark":true,"videoOutputInfo":[{"type":"DigitalVideoOutputDescriptor","outputType":"unknown","supportedHdcpVersions":[],"isHdcpEngaged":false}],"titleSpecificData":{"81167709":{"unletterboxed":true}},"preferAssistiveAudio":false,"isUIAutoPlay":true,"isNonMember":false,"desiredVmaf":"plus_lts","desiredSegmentVmaf":"plus_lts","requestSegmentVmaf":false,"supportsPartialHydration":true,"contentPlaygraph":["start"],"supportsAdBreakHydration":true,"liveMetadataFormat":"INDEXED_SEGMENT_TEMPLATE","useBetterTextUrls":true,"maxSupportedLanguages":2}}')
        local result0=$(curl -fsL --location 'https://www.netflix.com/nq/website/memberapi/vc2bfd55a/pathEvaluator?original_path=%2Fshakti%2Fmre%2FpathEvaluator' --header 'accept: */*' --header 'cache-control: no-cache' --header 'content-type: application/x-www-form-urlencoded' --header "$cookie" --data-urlencode 'path=["videos",70143836,["availability","inRemindMeList","queue"]]' --max-time 5 --connect-to www.netflix.com:443:$sniProxy:443 | tr -d '[:space:]' | grep '"isPlayable":false')
        if [[ -n "$result0" ]]; then
            echo -n -e "\r Netflix:\t\t\t\tFailed (Not Playable)\n"
            return
        fi
        local result_list=$(curl -fsL --location 'https://www.netflix.com/playapi/cadmium/manifest/1?reqAttempt=1&reqName=manifest&clienttype=akira&uiversion=vc2bfd55a&browsername=chrome&browserversion=118.0.0.0&osname=mac&osversion=10.15.7' --header 'accept: */*' --header 'cache-control: no-cache' --header 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36' --header 'Content-Type: text/plain' --header "$cookie" --data "$data" --max-time 5 --connect-to www.netflix.com:443:$sniProxy:443)
        local url=$(echo "$result_list" | grep -o 'https://[^"]*' | grep -m 1 'https://[^"]*')
        if [[ -n "$url" ]]; then
            local domain=$(echo "$url" | cut -d'/' -f3)
            local video_http_code=$(curl "$url" --user-agent "${UA_Browser}" -fsL --write-out %{http_code} --output /dev/null --max-time 5 --connect-to $domain:443:$sniProxy:443 2>&1 )
#            echo -n -e "Netflix: $video_http_code\n"
#            return
            if [[ "$video_http_code" != "200" ]] && [[ "$video_http_code" != "403" ]]; then
                echo -n -e "\r Netflix:\t\t\t\tFailed (Video Failed:$video_http_code)\n"
                return
            fi
        fi
        local region=$(curl $useNIC $usePROXY $xForward -${1} --user-agent "${UA_Browser}" -fs --max-time 5 --write-out %{redirect_url} --output /dev/null "https://www.netflix.com/title/80018499" --connect-to www.netflix.com:443:$sniProxy:443 2>&1 | cut -d '/' -f4 | cut -d '-' -f1 | tr [:lower:] [:upper:])
        if [[ ! -n "$region" ]]; then
            local region2=$(curl --max-time 5 -sL "https://api.country.is/$sniProxy" | tr -d '[:space:]' | grep -o '"country":"[^"]*' | cut -d':' -f2 | tr -d '"' 2>&1 )
            region="$region2"
        fi
        if [[ ! -n "$region" ]]; then
            region="US"
        fi
        echo -n -e "\r Netflix:\t\t\t\tYes (Region: [\"${region}\"])\n"
        return
    elif [[ "$result1" == "000" ]]; then
        echo -n -e "\r Netflix:\t\t\t\tFailed (Network Connection)\n"
        return
    fi
}


function MediaUnlockTest_SlingTV() {
    local tmpresult=$(curl --max-time 6 -sL 'https://p-geo.movetv.com/geo' --connect-to p-geo.movetv.com:443:$sniProxy:443 | tr -d '[:space:]' 2>&1)
    if [[ -z "$tmpresult" ]]; then
        echo -n -e "\r Sling TV:\t\t\t\tFailed (Network Connection)\n"
        return
    fi
    local playbackOk=$(echo $tmpresult | grep -o '"playback_disallowed":null')
    local networkOk=$(echo $tmpresult | grep -o '"access_allowed":null')
    local ipAllowed=$(echo $tmpresult | grep -o '"ip_restricted":null')
    local region=$(echo $tmpresult | grep -o '"country":"[^"]*' | cut -d':' -f2 | tr -d '"')
    if [[ -n "$networkOk" ]] && [[ -n "$ipAllowed" ]] && [[ "$region" == "usa" ]]; then
        echo -n -e "\r Sling TV:\t\t\t\tYes(Region: [\"US\"])\n"
    else
        echo -n -e "\r Sling TV:\t\t\t\tNo(access_allowed=$access_allowed ip_restricted=$ip_restricted playback_disallowed:$playback_disallowed) region=$region\n"
    fi
}


function MediaUnlockTest_HotStar() {
    local result=$(curl 'https://api.hotstar.com/o/v1/page/1557?offset=0&size=20&tao=0&tas=20' --connect-to api.hotstar.com:443:$sniProxy:443 --max-time 6 -fsL --write-out %{http_code} --output /dev/null 2>&1)
    if [ "$result" = "000" ]; then
        echo -n -e "\r Hotstar:\t\t\t\tFailed (Network Connection)\n"
        return
    elif [ "$result" = "401" ]; then
    
        local output=$(curl --max-time 6 -X GET -sSL 'https://www.hotstar.com' --connect-to www.hotstar.com:443:$sniProxy:443 -I)
        
        local region=$(echo "$output" | grep -m 1 -o 'geo=..' | cut -d '=' -f 2 | tr '[:lower:]' '[:upper:]')
        
        local site_region=$(echo "$output" | grep -oE 'location:.*\/(.*)$' | rev | cut -d'/' -f1 | rev | tr '[:lower:]' '[:upper:]'| tr -cd '[:alpha:]')
        
        if [ -n "$region" ] && [ "$region" = "$site_region" ]; then
            echo -n -e "\r Hotstar:\t\t\t\tYes (Region: [\"$region\"])\n"
            return
        else
            echo -n -e "\r Hotstar:\t\t\t\tNo\n"
            return
        fi
    elif [ "$result" = "475" ]; then
        echo -n -e "\r Hotstar:\t\t\t\tNo\n"
        return
    else
        echo -n -e "\r Hotstar:\t\t\t\tFailed\n"
    fi
}


function MediaUnlockTest_HuluUS() {
    local isUS=$(curl --max-time 6 -s "https://api.country.is/$sniProxy" | grep 'US')
    if [ -z "$isUS" ]; then
        echo -n -e "\r Hulu:\t\t\t\tNot America IP\n"
        return
    fi
    local _h_csrf_id=$(openssl rand -hex 32)
      local csrf_result=$(curl --max-time 6 'https://auth.hulu.com/v2/csrf?path=%2Fv4%2Fweb%2Fpassword%2Fauthenticate' -s --header "cookie: _h_csrf_id=$_h_csrf_id;" -H 'authority: auth.hulu.com' -H 'accept: */*' -H 'accept-language: zh-CN,zh;q=0.9' -H 'cache-control: no-cache' --connect-to auth.hulu.com:443:$sniProxy:443)
    local csrf_data=$(echo "$csrf_result" | grep -o '"csrf":"[^"]*"' | cut -d':' -f2 | tr -d '"')
    if [ -z "$csrf_data" ]; then
        echo -n -e "\r Hulu:\t\t\t\tNo\n"
        return
    fi
    
    local random_number=$(openssl rand -hex 6)
    local random_password=$(openssl rand -hex 3)
    local user_email="mc$random_number"
    local json=$(curl --max-time 6 'https://auth.hulu.com/v4/web/password/authenticate' -s -H 'authority: auth.hulu.com' -H 'accept: application/json' -H 'accept-language: zh-CN,zh;q=0.9' -H 'cache-control: no-cache' -H 'content-type: application/x-www-form-urlencoded; charset=utf-8' -H "cookie:_h_csrf_id=$_h_csrf_id;" -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36' --data-raw "user_email=harbourhulu@hotmail.com&password=lightsail$random_password%40&scenario=web_password_login&csrf=$csrf_data" --compressed --connect-to auth.hulu.com:443:$sniProxy:443)
    local playlist=$(curl --max-time 6 -s --location 'https://play.hulu.com/v6/playlist' --header 'Accept: */*' --header 'Content-type: application/json' --header 'Cookie: _hulu_session=%2FEABkgv1kvW4mdrGBYrSNdNKMl0-IhGqA3y%2FhDJUIArmXm59mw--wCuHjVvaAeE%2Fw0xgLW1Cd1hYxx8VgrFs4skD07kAnjAwTBpTl6Fz_WIA78g7B%2FZnPPLUodJyS04Ckp_UrrkeLzyGHiSb8oTsCj2rTpwG7KLFL%2Fq7_ZZNUzF3GOyaUX3IDSX7BwDbjjbtpKRrifIqF_d163VF8wdikycTR3PFLnCKZZ%2FqWyWTcG336nmGQUCF0LPhh9HlZNP0tQ82hh%2FBMuyRMygnuoOpKdvvusIsd2ypxjZikHqHn7RXV%2F4pQhakslrvxM4NFRMS_50vFqaWJTK3O3_dDXogWhv19x6S753QUtl6wpXhezsjRrXbn9Q1Old7SIMtOzyblbNZpvT4o_3722A5FyeV1CkzAQxsDZfhTYuGB_H3KL6vUH%2F1RQaAK57vt6xrurH3CavhsDKkRXrX20wzHjNNOlDu5kHmIDlZsas8uujRGkKJ4LdlfJxHsINtv6HyMsMkfjP3PHQ8S%2Ff9ekA02bNXQhwhoLMREJaL7c6pqYJKEex3ZmC3q2UMk2k2YnRDgDjh7TUNot3N0uDKsxxSZDaK8QvCeLTeRgB1SQ3mP_dvZu_U3_QsmeoOnpAbe8IXo5bx92bBdZ4VsMhSuZhHi8oMiCelt0W8VCvSvT__Q78D7Mg57l4HKhESR_uEh90upQVM%2FBpVhfpe6Sf_Ltb1Nlak6Xs9XDE0bR1F_YMqcz4Kk5zBAIHZ6KmTIG58%2FVMBEOWwmW3OJOS2WVbaGmCNY7X8jPkLCbbFrcyKnguPWgJy0S2OKtwI2cHayjP4V0YXsYnyxCMv_c2W4C33eQeHl5aHWt85CF21%2FsJGDrk5QWBZQC7VIogzKivuHEbviz_MAEXvZ5mngNaW09ji4cm_6u_WDEc9Jq_x6j8jfnw1HNgcS4WDuY%2FDtkFQgrfdiSlswQglbNkohBcVFcbUs8816KS4DZIo5hOq_Fo24MGF2GGSWofeii3wZTo4K_IXfGt%2F2QKqM1pmue8QxsLSUwPH36oe9a66rfDG6vZ_sIyc8sErJENF12mV3QGWiDbl_a5QUa2VO3gbNu9wuwF3ZFpfMvSnyJC6aPW1FEBXcp9D0qou%2F4x%2FdIckR_MtxFuEPYrvIIaknemaVnlhEEExM_sF5Ba8L2fgaApDK3MOliSYtnKoc%2FiL7SZf4bwJX8lf_jsBxRbJZTc1JXPzT9KAnkQ2ui5QAYWF_b5CSrro89K%2F5YfifvfQ%2FxWnHieqx56xWMyif5tJxjV0O004UNfqqAj5zzLs%2F6ngzvtFjIhES0PzBjp4DifcT99wkDbuE6QnWcxi34yAdtQmuuDpP23yhOFhGx6lb507xLzo9K9Pb2RQdpk7fqKRCjVyuHzmkU1biw0dF81GparB2kVn4Y74vOzXQ1sPQR328Oi4qY_XxjGeN_eOq8LYIke9gHQ5IpvUp7uK6XN5eXLMx3WmIYpgoJU17cQErMYcuvj1ICx6PXLDbYsWuJTZQNiVl2HuEY_t9JMT5ASoxmH7Iq3cF5skFD3zbXSc9uYoI9gw86U%2FH6r_fCDU15Eq8LWFSYsywSoZVTvkBFbK2EmcnzGVVBlG9BO1O1ELzUnIm%2FphyS8-;' --data '{"device_identifier":"8A4BF73166C4EEE297281C6396A0B3D1","deejay_device_id":214,"version":1,"all_cdn":true,"content_eab_id":"EAB::5ced87d7-2db5-4f6c-a55f-c51f3a68d371::171251201::199753627","region":"US","xlink_support":false,"device_ad_id":"84AF37B1-F2E4-3C1F-26F4-3E072343444E","limit_ad_tracking":false,"privacy_opt_out":"NO","ignore_kids_block":false,"language":"en","guid":"8A4BF73166C4EEE297281C6396A0B3D1","rv":340928,"kv":471294,"unencrypted":true,"include_t2_revenue_beacon":"1","include_t2_rev_beacon":true,"include_t2_adinteraction_beacon":true,"cp_session_id":"81C6B056-F4DA-F7D6-2A1C-DCA6A23D9738","interface_version":"1.14.3","network_mode":"wifi","is_html5_player":true,"support_js":true,"support_vpaid":true,"play_intent":"resume","lat":22.2487292,"long":113.5693722,"playback":{"version":2,"video":{"codecs":{"values":[{"type":"H264","width":1920,"height":1080,"framerate":60,"level":"4.2","profile":"HIGH"}],"selection_mode":"ONE"}},"audio":{"codecs":{"values":[{"type":"AAC"}],"selection_mode":"ONE"}},"drm":{"values":[{"type":"WIDEVINE","version":"MODULAR","security_level":"L3"},{"type":"PLAYREADY","version":"V2","security_level":"SL2000"}],"selection_mode":"ALL"},"manifest":{"type":"DASH","https":true,"multiple_cdns":true,"patch_updates":true,"hulu_types":true,"live_dai":true,"multiple_periods":true,"xlink":false,"secondary_audio":true,"unified_asset_signaling":false,"live_fragment_delay":3},"segments":{"values":[{"type":"FMP4","encryption":{"mode":"CENC","type":"CENC"},"https":true}],"selection_mode":"ONE"}}}' --connect-to play.hulu.com:443:$sniProxy:443)
    local is_playable=$(echo "$playlist" | grep 'BYA-403-011')
    if [ -n "$is_playable" ]; then
        echo -n -e "\r Hulu:\t\t\t\tNo\n"
        return
    fi
    local isOk=$(echo $json | grep 'Please check your email and password and try again')
    
    if [ -n "$isOk" ]; then
        echo -n -e "\r Hulu:\t\t\t\tYes (Region: [\"US\"])\n"
        return
    else
        echo -n -e "\r Hulu:\t\t\t\tNo\n"
        return
    fi
}


function MediaUnlockTest_BBCiPLAYER() {
    local tmpresult5=$(curl -fsL --write-out %{http_code} --output /dev/null --max-time 6 'https://www.bbc.co.uk/favicon.ico' --connect-to www.bbc.co.uk:443:$sniProxy:443)
    if [ "$tmpresult5" != "200" ]; then
        echo -n -e "\r BBC:\t\t\t\tFailed\n"
        return
    fi
    local tmpresult3=$(curl --max-time 6 -s 'https://www.bbc.co.uk/userinfo' --connect-to www.bbc.co.uk:443:$sniProxy:443)
    local xCountry=$(echo "$tmpresult3" | grep -o '"X-Country": *"[^"]*' | cut -d'"' -f4)
    local xIsOk=$(echo "$tmpresult3" | grep -o '"X-Ip_is_uk_combined": *"[^"]*' | cut -d'"' -f4)
    if [[ "$xCountry" != "gb" || "$xIsOk" != "yes" ]]; then
        echo -n -e "\r BBC:\t\t\t\tFailed\n"
        return
    fi
    local tmpresult4=$(curl -fsL --write-out %{http_code} --output /dev/null --max-time 6 'https://iplayer-web.files.bbci.co.uk/page-builder/51.1.0/img/icons/favicon.ico' --connect-to iplayer-web.files.bbci.co.uk:443:$sniProxy:443)
    if [ "$tmpresult4" != "200" ]; then
        echo -n -e "\r BBC:\t\t\t\tFailed\n"
        return
    fi
    local tmpresult6=$(curl -fsL --write-out %{http_code} --output /dev/null --max-time 6 'https://mm.bidi.bbc.co.uk/vod-dash-uk/usp/auth/vod/piff_abr_full_hd/d4298b-p0chtd9n/vf_p0chtd9n_07847271-fe6e-4315-a814-0d6e7fc7baee.ism/pc_hd_abr_v2_dash_master.mpd?at=NeE2jVX42fdf82666b12a98bc690446bf288ed7d47daf6e3603c1970c4880' --connect-to mm.bidi.bbc.co.uk:443:$sniProxy:443 2>&1)
    if [[ "${tmpresult6}" = "000" ]]; then
        echo -n -e "\r BBC:\t\t\t\tFailed (Network Connection)\n"
        return
    fi
    local tmpresult1=$(curl -sSL --max-time 6 "https://open.live.bbc.co.uk/mediaselector/6/select/version/2.0/mediaset/pc/vpid/p0chskk0/format/json/cors/1" --connect-to open.live.bbc.co.uk:443:$sniProxy:443 2>&1)
    if [ "$tmpresult1" = "000" ]; then
        echo -n -e "\r BBC:\t\t\t\tFailed (Network Connection)\n"
        return
    fi
    if [ -n "$tmpresult1" ]; then
        result=$(echo $tmpresult | grep 'geolocation')
        if [ -n "$result" ]; then
            echo -n -e "\r BBC:\t\t\t\tNo\n"
        else
            echo -n -e "\r BBC:\t\t\t\tYes (Region: [\"GB\"])\n"
        fi
    else
        echo -n -e "\r BBC:\t\t\t\tFailed\n"
    fi
}


function MediaUnlockTest_FuboTV() {
    local tmpresult=$(curl --max-time 6 -sL 'https://api.fubo.tv/v3/location' --connect-to api.fubo.tv:443:$sniProxy:443 2>&1)
    if [[ "$tmpresult" == "curl"* ]]; then
        echo -n -e "\r fuboTV:\t\t\t\tFailed (Network Connection)\n"
        return
    fi
    
    local networkOk=$(echo $tmpresult | grep '"network_allowed":true')
    local sameIp=$(echo $tmpresult | grep -o '"ip_address":"[^"]*' | cut -d':' -f2 | tr -d '"')
    local region=$(echo $tmpresult | grep -o '"country_code2":"[^"]*' | cut -d':' -f2 | tr -d '"')
    if [[ -n "$networkOk" ]] && [[ $sameIp == $sniProxy ]]; then
        echo -n -e "\r fuboTV:\t\t\t\tYes(Region: [\"$region\"])\n"
    else
        echo -n -e "\r fuboTV:\t\t\t\tNo\n"
    fi
}


function OpenAITest(){
    local result2=$(curl $useNIC $usePROXY $xForward -${1} -sL --max-time 5 'https://ios.chat.openai.com/auth/login' --connect-to ios.chat.openai.com:443:$sniProxy:443 2>&1)
    local block_country=$(echo "$result2" | grep 'unsupported_country')
    local available_mark=$(echo "$result2" | grep 'Request is not allowed. Please try again later.')
    if [[ -z "$block_country" && -n "$available_mark" ]]; then
        echo -n -e "\r ChatGPT:\t\t\t\tYes (Region: [\"US\"])\n"
        return
    else
        echo -n -e "\r ChatGPT:\t\t\t\tNo(reason=$block_country)\n"
        return
    fi
}

function MediaUnlockTest_ABC() {
    local result1=$(curl $useNIC $usePROXY $xForward -${1} --user-agent "${UA_Browser}" -fsL --write-out %{http_code} --output /dev/null --max-time 5 "https://prod.gatekeeper.us-abc.symphony.edgedatg.com/api/ws/profile/v1/profiles/868fdcae-e94e-4da9-a8c6-159ac0b19bdf/brands/001/lists/favorites" --connect-to prod.gatekeeper.us-abc.symphony.edgedatg.com:443:$sniProxy:443 2>&1)
    if [ "$result1" == "000" ]; then
        echo -n -e "\r ABC:\t\t\t\tFailed (Network Connection symphony.edgedatg.com)\n"
        return
    fi
    
    local tmpresult=$(curl 'https://prod.gatekeeper.us-abc.symphony.edgedatg.go.com/vp2/ws/utils/2021/geo/video/geolocation/001/001/gt/-1.jsonp' -sSL --max-time 10 --connect-to prod.gatekeeper.us-abc.symphony.edgedatg.go.com:443:$sniProxy:443 | tr -d '[:space:]' 2>&1)
    if [ -z "$tmpresult" ]; then
        echo -n -e "\r ABC:\t\t\t\tFailed (Network Connection symphony.edgedatg.go.com)\n"
        return
    fi

    local result=$(echo $tmpresult | grep '"allowed":true')
    local region=$(echo $tmpresult | grep -o '"country":"[^"]*' | cut -d':' -f2 | tr -d '"')
    if [[ -n "$result" ]] && [[ "$region" == "usa" ]] ; then
        local tmpresult2=$(curl 'https://prod.gatekeeper.us-abc.symphony.edgedatg.go.com/vp2/ws-secure/entitlement/2020/playmanifest_secure.json' -sSL --max-time 10 --connect-to prod.gatekeeper.us-abc.symphony.edgedatg.go.com:443:$sniProxy:443 | tr -d '[:space:]' 2>&1)
        if [ -z "$tmpresult2" ]; then
            echo -n -e "\r ABC:\t\t\t\tFailed (Network Connection)\n"
            return
        fi
        local result2=$(echo $tmpresult2 | grep 'outside the United States')
        if [ -n "$result2" ]; then
            echo -n -e "\r ABC:\t\t\t\tNo(outside the United States)\n"
            return
        fi
        echo -n -e "\r ABC:\t\t\t\tYes(Region: [\"US\"])\n"
        return
    else
        echo -n -e "\r ABC:\t\t\t\tNo($region $result)\n"
    fi

}

function MediaUnlockTest_ESPNPlus() {
    local espncookie=$(echo "$Media_Cookie" | sed -n '11p')
    local TokenContent=$(curl -${1} --user-agent "${UA_Browser}" -s --max-time 6 -X POST "https://espn.api.edge.bamgrid.com/token" -H "authorization: Bearer ZXNwbiZicm93c2VyJjEuMC4w.ptUt7QxsteaRruuPmGZFaJByOoqKvDP2a5YkInHrc7c" -d "$espncookie" --connect-to espn.api.edge.bamgrid.com:443:$sniProxy:443 2>&1)
    local isBanned=$(echo $TokenContent | python -m json.tool 2>/dev/null | grep 'forbidden-location')
    local is403=$(echo $TokenContent | grep '403 ERROR')

    if [ -n "$isBanned" ] || [ -n "$is403" ]; then
        echo -n -e "\r ESPN+:\t\t\t\tNo(is403=$is403, isBanned=$isBanned)\n"
        return
    fi

    local fakecontent=$(echo "$Media_Cookie" | sed -n '10p')
    local refreshToken=$(echo $TokenContent | python -m json.tool 2>/dev/null | grep 'refresh_token' | awk '{print $2}' | cut -f2 -d'"')
    local espncontent=$(echo $fakecontent | sed "s/ILOVESTAR/${refreshToken}/g")
    local tmpresult=$(curl -${1} --user-agent "${UA_Browser}" -X POST -sSL --max-time 6 "https://espn.api.edge.bamgrid.com/graph/v1/device/graphql" -H "authorization: ZXNwbiZicm93c2VyJjEuMC4w.ptUt7QxsteaRruuPmGZFaJByOoqKvDP2a5YkInHrc7c" -d "$espncontent" --connect-to espn.api.edge.bamgrid.com:443:$sniProxy:443 2>&1)

    if [[ "$tmpresult" == "curl"* ]]; then
        echo -n -e "\r ESPN+:\t\t\t\tFailed (Network Connection)\n"
        return
    fi
    
    local region=$(echo $tmpresult | python -m json.tool 2>/dev/null | grep 'countryCode' | cut -f4 -d'"')
    local inSupportedLocation=$(echo $tmpresult | python -m json.tool 2>/dev/null | grep 'inSupportedLocation' | awk '{print $2}' | cut -f1 -d',')

    if [[ "$region" == "US" ]] && [[ "$inSupportedLocation" == "true" ]]; then
        echo -n -e "\r ESPN+:\t\t\t\tYes(Region: [\"US\"])\n"
        return
    else
        echo -n -e "\r ESPN+:\t\t\t\tNo(inSupportedLocation=$inSupportedLocation region=$region)\n"
        return
    fi
}

function MediaUnlockTest_Omegle() {
    local checkCode=$(curl --location --request POST 'https://waw1.omegle.com/check' --header 'accept: */*' --header 'origin: https://www.omegle.com' --header 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36' -sSL --max-time 6  --connect-to waw1.omegle.com:443:$sniProxy:443)
    if [ -z "$checkCode" ]; then
        echo -n -e "\r Omegle:\t\t\t\tFailed (Network Connection)\n"
        return
    fi
    local tmpresult=$(curl --location --request POST "https://front35.omegle.com/start?caps=recaptcha2%2Ct3&firstevents=1&spid=&randid=VJ22RG3L&cc=$checkCode&lang=zh" --header 'authority: front35.omegle.com' --header 'accept: application/json' --header 'accept-language: zh-CN,zh;q=0.9,en;q=0.8' --header 'cache-control: no-cache' --header 'content-length: 0' --header 'content-type: application/x-www-form-urlencoded; charset=UTF-8' --header 'origin: https://www.omegle.com' --header 'pragma: no-cache' --header 'referer: https://www.omegle.com/' --header 'sec-ch-ua: "Chromium";v="118", "Google Chrome";v="118", "Not=A?Brand";v="99"' --header 'sec-ch-ua-mobile: ?0' --header 'sec-ch-ua-platform: "macOS"' --header 'sec-fetch-dest: empty' --header 'sec-fetch-mode: cors' --header 'sec-fetch-site: same-site' --header 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36' -sSL --max-time 6 --connect-to front35.omegle.com:443:$sniProxy:443 | tr -d '[:space:]' 2>&1)
    if [ -z "$tmpresult" ]; then
        echo -n -e "\r Omegle:\t\t\t\tFailed (Network Connection)\n"
        return
    fi

    local result=$(echo $tmpresult | grep '"waiting"')
    if [[ -n "$result" ]]; then
        local region2=$(curl --max-time 10 -sL "https://api.country.is/$sniProxy" | tr -d '[:space:]' | grep -o '"country":"[^"]*' | cut -d':' -f2 | tr -d '"' 2>&1 )
        if [ -z "$region2" ]; then
            region2=$(curl --max-time 10 -sL "http://ip-api.com/json/$sniProxy" | tr -d '[:space:]' | grep -o '"countryCode":"[^"]*' | cut -d':' -f2 | tr -d '"' 2>&1)
            if [ -z "$region2" ]; then
                echo -n -e "\r Omegle:\t\t\t\tFailed (No Country)\n"
                return
            fi
        fi
        echo -n -e "\r Omegle:\t\t\t\tYes(Region: $region2)\n"
        return
    else
        echo -n -e "\r Omegle:\t\t\t\tNo\n"
    fi
}

function MediaUnlockTest_AbemaTV_IPTest() {
    local tempresult=$(curl $useNIC $usePROXY $xForward --user-agent "${UA_Dalvik}" -${1} -fsL --write-out %{http_code} --max-time 6 "https://abema.tv" --connect-to abema.tv:443:$sniProxy:443 2>&1)
    if [ "$tempresult" == "000" ] || [ "$tempresult" == "503" ]; then
        echo -n -e "\r ABEMA:\t\t\t\tFailed (Network Connection abema.tv)\n"
        return
    fi
    
    local regionDetect=$(curl "https://ds-linear-abematv.akamaized.net/region" -sSL --max-time 6 --connect-to ds-linear-abematv.akamaized.net:443:$sniProxy:443 | tr -d '[:space:]' 2>&1)
    if [ "$regionDetect" != "OK" ]; then
            echo -n -e "\r ABEMA:\t\t\t\tNo (Ip Blocked)\n"
        return
    fi
    
    local test1=$(curl $useNIC $usePROXY $xForward --user-agent "${UA_Dalvik}" -${1} -fsL --write-out %{http_code} --max-time 6 "https://api.p-c3-e.abema-tv.com/v1/users/CmiufN4aWJ8M1h/trialAvailable?planId=tv.abema.sub.credit" --connect-to api.p-c3-e.abema-tv.com:443:$sniProxy:443 2>&1)
    if [[ "$test1" == "000" ]]; then
        echo -n -e "\r ABEMA:\t\t\t\tFailed (Network Connection abema-tv.com)\n"
        return
    fi

    echo -n -e "\r ABEMA:\t\t\t\tYes(Region: [\"JP\"])\n"
}

function MediaUnlockTest_TubiTV() {
    local tmpresult=$(curl $useNIC $usePROXY $xForward -${1} ${ssll} -sS --user-agent "${UA_Browser}" --max-time 10 "https://tubitv.com/home" --connect-to tubitv.com:443:$sniProxy:443 2>&1)
    if [[ "$tmpresult" == "curl"* ]]; then
        echo -n -e "\r Tubi:\t\t\t\tFailed (Network Connection)\n"
        return
    fi
    local result=$(echo "$tmpresult" | grep '302 Found')
    if [ -n "$result" ]; then
        echo -n -e "\r Tubi:\t\t\t\tNo\n"
    else
        local region2=$(echo "$tmpresult" | tr -d '[:space:]' | grep -o '"twoDigitCountryCode":"[^"]*' | cut -d':' -f2 | tr -d '"')
        if [ -z "$region2" ]; then
            echo -n -e "\r Tubi:\t\t\t\tNo\n"
        fi
        echo -n -e "\r Tubi:\t\t\t\tYes(Region: [\"$region2\"])\n"
    fi
}

function MediaUnlockTest_FOXSports() {
    local result=$(curl --max-time 6 'https://prod.api.video.fox/v2.0/watch' --header 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36' --header 'x-api-key: cf289e299efdfa39fb6316f259d1de93' --header 'Content-Type: text/plain' --data '{"capabilities":["drm/widevine","fsdk/yo/v3"],"deviceWidth":790,"deviceHeight":857,"maxRes":"720p","os":"macos","osv":"","provider":{"freewheel":{"did":"4ca96db0-4825-7b53-e154-f38dfa098787"},"vdms":{"rays":""}},"playlist":"","privacy":{"us":"1YNN","lat":false},"siteSection":null,"streamType":"live","streamId":"FS2-program-27333715","debug":{"traceId":null}}' -s --connect-to prod.api.video.fox:443:$sniProxy:443)
    
    if echo "$result" | grep -q '"show ended"'; then
        echo -n -e "\r FOX Sports:\t\t\t\tYes (Region: [\"US\"])\n"
        return
    elif echo "$result" | grep -q '"name":"NotEntitled"'; then
        echo -n -e "\r FOX Sports:\t\t\t\tYes (Region: [\"US\"])\n"
        return
    else
        echo -n -e "\r FOX Sports:\t\t\t\tFailed\n"
    fi
}

function MediaUnlockTest_SonyLIV() {
    local result=$(curl -s --max-time 6 'https://apiv2.sonyliv.com/AGL/1.4/A/ENG/WEB/ALL/USER/ULD' --connect-to apiv2.sonyliv.com:443:$sniProxy:443)
    if echo "$result" | grep -q '"Geoblocked Country"'; then
        echo -n -e "\r SonyLIV:\t\t\t\tFailed(GeoBlocked)\n"
        return
    elif echo "$result" | grep -q 'pass the JWT validation'; then
        local result2=$(curl -s --max-time 6 'https://www.sonyliv.com' --connect-to www.sonyliv.com:443:$sniProxy:443)
        region=$(echo "$result2" | grep -o 'country_code:"[^"]*' | cut -d'"' -f2)
        if [ -n "$region" ]; then
            echo -n -e "\r SonyLIV:\t\t\t\tYes (Region: [\"$region\"])\n"
        else
            echo -n -e "\r SonyLIV:\t\t\t\tFailed(No Country)\n"
        fi
    else
        echo -n -e "\r SonyLIV:\t\t\t\tFailed\n"
    fi
}

function MediaUnlockTest_TrueID() {
   local result=$(curl --max-time 6 'https://movie.trueid.net/apis/auth/checkedplay' -H 'authority: movie.trueid.net' -H 'accept: application/json, text/plain, */*' -H 'accept-language: zh-CN,zh;q=0.9,en;q=0.8' -H 'authorization: Basic OTkzNzE3YjAyNGYzMjRmMTc0YjAwNjA0MDg1NGIzOTdjNTM1Y2M1MDpmMzI0ZjE3NGIwMDYwNDA4NTRiMzk3YzUzNWNjNTA=' -H 'cache-control: no-cache' -H 'content-type: application/json' -H 'cookie: visid_incap_2689818=ANHpsBEuSvScX5GN3froM3aJ5mUAAAAAQUIPAAAAAAAobNz6YB3pR2cjcIX2X9s3; visid_incap_2104120=ezY+5rzuRJ2Dt8fcS2vCLROJ5mUAAAAAQUIPAAAAAADqmLmVb7daLbQBqYEfF72r; visid_incap_2689830=FfOYE4EzSFi5Q4/B1nL/KoWJ5mUAAAAAQUIPAAAAAABXwPiA71vKsLLbWPF+XV2q; _gid=GA1.2.1993013949.1709607302; visid_incap_2679318=tEY9fWbeSS69oT/bVGr5+IWJ5mUAAAAAQUIPAAAAAAAzb4zP8ykm8jUKlgNTsjft; unique_user_id=1879419412.1709607302; _gcl_au=1.1.844502069.1709607303; _uid26068=2A90F590.1; _fbp=fb.1.1709607304271.52892651; _tt_enable_cookie=1; _ttp=4lx6M1i91BzhE2cCGxoPkLXspQC; __lt__cid=05ed0228-91d9-4dfb-aaad-04f1108c9daf; afUserId=bf620a62-c4f5-4e03-be3f-ae39ca21ed1b-p; AF_SYNC=1709607305849; visit_time=801; incap_ses_134_2689818=EexRBc2cSDJplzEjiRDcAXU252UAAAAAWjpzyUWQ+UyYhtwfbvUGuQ==; nlbi_2689818=jp5KPtvL/FtAtexioxGZcwAAAAATIq8/9MzvADLAzhnDXeX2; incap_ses_715_2689818=LlwTfp4BVjxdcqWc1DHsCec352UAAAAAsFrTxOaUvbCNo0xf3RQ0qQ==; incap_ses_1192_2104120=pFJhHbssJyk8vngpCteKEOc352UAAAAAP4N3Ef7UrfYy7gbU4Aug3Q==; incap_ses_714_2689830=ZtNoDHca+hvdExhB0aPoCfo352UAAAAAUTov1Uky4vx6REm/D3lnOw==; 9116882c9256e8fa3f2971a3c9dd81c0=f95c5c9179bb8944477ff61e412cfa01; nlbi_2689830=tnq1NrmnMyJRCQDF5YqcVwAAAAD2AK44rW8Wy1s9fhAoCqyV; incap_ses_33_2689830=9gDEHH5fd3KbqWbEpD11AJnl52UAAAAAzLzZE/gRBhYVuYTL3tLikA==; incap_ses_33_2679318=TPBXKpwE52vVemfEpD11AOrl52UAAAAA1h88neFRlJr5u898jYRGYA==; _clck=bxkjld%7C2%7Cfju%7C0%7C1526; _clsk=da47ty%7C1709696493542%7C1%7C1%7Cu.clarity.ms%2Fcollect; visid_incap_2793097=iAaCWthdTWm6s77uEO+UEgLm52UAAAAAQUIPAAAAAAC5rV17WGCMOeuq7cAojLvK; incap_ses_626_2793097=hZZkMdqx5GhumSNI4gCwCALm52UAAAAAZNo2fhND41zsp3F0GieTLA==; visid_incap_2624018=gnJEVVb+T1GFewGWmali/Cbm52UAAAAAQUIPAAAAAABEuKE8lqn9OfIt1B0oAoWV; incap_ses_1204_2624018=EOCeG63DAUZhva41/Hi1ECbm52UAAAAAqZJQA+iTIBq2jy/Y4AnbWQ==; incap_ses_1204_2736462=5zbkRZ0N8iWvm7M1/Hi1ECvu52UAAAAAAfzMAWATvp/GyHdkZJsTew==; visid_incap_2736462=ZZn9us9pRBeApuOnKr13Vvfb52UAAAAAQ0IPAAAAAACAgcyyAbb/IA6vADBuiwOD1JjRZZaWztnx; incap_ses_1204_2679318=Prh1PQWdrkxQqbM1/Hi1EEPu52UAAAAA2/Uw+7jKzi7IbiYhBoLrXQ==; incap_ses_428_2689818=xAVfKT0k9xXmi08n8ZDwBYvu52UAAAAAOR3vQfcyzWIhqBs9Qta1Pg==; incap_ses_573_2104120=rEtYcxPUb3x6ABVcurTzB4vu52UAAAAAhgMBXfolVkhFzphqIrcluw==; incap_ses_626_2689830=T5+lQ8tslEfHSidI4gCwCJru52UAAAAAjsBLnWP/01od09tsVlBpOQ==; _gat_UA-86733131-1=1; OptanonConsent=isIABGlobal=false&datestamp=Wed+Mar+06+2024+12%3A18%3A35+GMT%2B0800+(China+Standard+Time)&version=6.13.0&hosts=&landingPath=NotLandingPage&groups=C0001%3A1%2CC0002%3A1%2CC0003%3A1%2CC0004%3A1%2CC0005%3A1&AwaitingReconsent=false&geolocation=%3B; OptanonAlertBoxClosed=2024-03-06T04:18:35.671Z; __lt__sid=1455b30a-7d02e5e2; sessioncenter=s%3AmSADDlCwKFHvxpyEXGarvgkaO68o6Nko.g3anBGU5gDtGWywCPz%2FjI1mt42mTXmD0YR4sKNPYAvA; _ga_R05PJC3ZG8=GS1.1.1709696489.8.1.1709698717.8.0.0; _ga=GA1.1.1879419412.1709607302' -H 'origin: https://movie.trueid.net' -H 'pragma: no-cache' -H 'referer: https://movie.trueid.net/embed/kw5oQ5GKL5Jy?content_type=sportclip&autoplay=true' -H 'sec-ch-ua: "Chromium";v="122", "Not(A:Brand";v="24", "Google Chrome";v="122"' -H 'sec-ch-ua-mobile: ?0' -H 'sec-ch-ua-platform: "macOS"' -H 'sec-fetch-dest: empty' -H 'sec-fetch-mode: cors' -H 'sec-fetch-site: same-origin' -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36' --data-raw '{"lang":"th","cmsId":"kw5oQ5GKL5Jy","contentType":"sportclip"}' --connect-to movie.trueid.net:443:$sniProxy:443 -s)
    if echo "$result" | grep -q '"GEO_BLOCK"'; then
        echo -n -e "\r TrueID:\t\t\t\tNo (geo block)\n"
        return
    elif echo "$result" | grep -q '"isPlay":true'; then
        echo -n -e "\r TrueID:\t\t\t\tYes (Region: [\"TH\"])\n"
    elif echo "$result" | grep -q 'ขออภัยค่ะ ไม่สามารถใช้งานได้ในขณะนี้'; then
        echo -n -e "\r TrueID:\t\t\t\tYes (Region: [\"TH\"])\n"
    else
        echo -n -e "\r TrueID:\t\t\t\tNo (failed)\n"
    fi
}

function MediaUnlockTest_F1tvpro() {
   local result=$(curl --max-time 6 --location 'https://f1tv.formula1.com/1.0/R/ENG/WEB_DASH/ALL/USER/LOCATION' --header 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36' --header 'x-f1-device-info: device=web;screen=browser;os=mac os;browser=chrome;browserVersion=122.0.0.0;model=Macintosh;osVersion=14.2.1;appVersion=release-R29.0.3;playerVersion=8.129.0' --connect-to f1tv.formula1.com:443:$sniProxy:443 -s)
    if echo "$result" | grep -q '"detectedCountryIsoCode":"US"'; then
        echo -n -e "\r F1 TV:\t\t\t\tYes (Region: [\"GB\",\"AU\",\"HK\",\"FR\",\"DE\",\"ID\",\"IT\",\"JP\",\"MY\",\"PH\",\"TW\",\"TH\",\"KR\"])\n"
        return
    else
        echo -n -e "\r F1 TV:\t\t\t\tNo (geo block)\n"
    fi
}

function MediaUnlockTest_Channel4() {
   local result=$(curl --max-time 6 'https://www.channel4.com/vod/stream/44564-001' --header 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36' --header 'Cookie: _abck=06ECF522B66ECAE1577AF91C24B2F273~-1~YAAQLXwhF75+buWNAQAAk0W3MQu8AMVuL/T/BQDKqMr4MXAYQcwt8YAmEVF83ZhqolZNM8JVnU302vobjxqyQ9jq3wj+boQZYkFnDta4Eu45c/Rc5IOqUkOPfDV9ClqZ1X3Hcb0lc7D8KZDj8aNHSnnFKYxMthOfYWaKBCdBrNbuqQBRz0ENlgcnu8viRUav6T9rnB0CzskEedM3gZSWjjzHmjp4VlcNGw4CbhEp1TcdSal8+KpN1Co2vXBoGKgXKPIgL7W11AEndQTTgut3kMrrANKWXaauyNJgYkAq7X2FGRilQUpj75i7CcHcVk6gt8mlzNZ5UvWWKxK9qJibHKXTVv6NEcATFdZQt2gUffIkivvAyGtyL/22Zf10Aw==~-1~-1~-1; ak_bmsc=07770D93F5BCA8B43FBA84214C88BA0A~000000000000000000000000000000~YAAQLXwhF79+buWNAQAAk0W3MRevrwdjr6Du7RcqWdalK4sIeMcUGgnRxsrUrWVP59IZQqR37l3b5rWVP6pKqcZCfeqMuwNPJ3adp8a3WRWnPUbhtnqQCxk4b9t2e0sWRG+G4Be26g2cZkU4TjLMpuQBdyrwq8hIW0HvuIO+cGIhwqKKQm/k/QIWhCnDL8qKI9UDj9d3bHiy8zTXNx4kKHzJjom4x2AjmOZWK4EtQy6njyGcfaIVJ2GC8++7NSINigSOyVjHHdO+ZIOcSIIOE1zuwa37cYNHPeOrG1MjYcqF46kmjO/4ASAjmtlcFayC/1ztWK4MqgJ1OGxmFH6MaGGySqL0AOjrVCecSk7YRnJN5oT9hcQ9vt5S; bm_sz=0A93BE4211BC8A66014B686C8FF87C99~YAAQLXwhF8B+buWNAQAAk0W3MRf3CutJuAYFaD5nMc7+OpG/4cXePfa4gsWZihNWHWNxJX66VoEWiKqzqOK7Ft38U6sNw3lZG5lv37gmlVKXOX91/KjqeuHqURYDD2XtsYBFIOdrIe9zxfFpYQUkMYxRHYj6Qvw0seuuNhJ7pLHaDk5LpQALfJGqIt+ZWe4FXoGHzSdc/DWX6Kt1b5+il9N5zAinTBlwxnjS1FwvB8lTMVITSkUWWykX9169itP94hBYG/Kqb8Kv9OWD4FJf812VZDDktEpoTcZNmWeobKedyx8ciIGUVgiHH0caQKeC0LxleaUtpI22ZNrAmfmKAN15y25briQLf389uA==~3294003~3420738' --connect-to www.channel4.com:443:$sniProxy:443 -s)
    if echo "$result" | grep -q 'playback blocked'; then
        echo -n -e "\r Channel 4:\t\t\t\tNo (playback block)\n"
    elif echo "$result" | grep -q '"The Big Bang Theory"'; then
        echo -n -e "\r Channel 4:\t\t\t\tYes (Region: [\"GB\"])\n"
    else
        echo -n -e "\r Channel 4:\t\t\t\tNo (network failed)\n"
    fi
}

function MediaUnlockTest_Max() {
    local result=$(curl --max-time 6 -s https://www.max.com/js/getGeoRedirect.js -I --connect-to www.max.com:443:$sniProxy:443)
#    set-cookie: countryCode=HK; expires=Thu, 14-Mar-2024 21:32:37 GMT; path=/
    local region=$(echo "$result" | grep -o 'countryCode=[^;]*' | cut -d'=' -f2 | tr '[:upper:]' '[:lower:]')
    local region2=$(echo "$region" | tr '[:lower:]' '[:upper:]')
    local result2=$(curl --max-time 6 -s https://www.max.com/js/getGeoRedirect.js --connect-to www.max.com:443:$sniProxy:443)
    if echo "$result2" | grep -q "\"$region\""; then
        echo -n -e "\r Max:\t\t\t\tYes (Region: [\"$region2\"])\n"
    else
        echo -n -e "\r Max:\t\t\t\tNo (failed)\n"
    fi
}

function MediaUnlockTest_DiscoveryPlus() {
    local GetToken=$(curl -sS --max-time 6 "https://us1-prod-direct.discoveryplus.com/token?deviceId=d1a4a5d25212400d1e6985984604d740&realm=go&shortlived=true" -connect-to us1-prod-direct.discoveryplus.com:443:$sniProxy:443 2>&1)
    if [[ "$GetToken" == "curl"* ]] && [[ "$1" == "6" ]]; then
        echo -n -e "\r discovery+:\t\t\t\tIPv6 Not Support\n"
        return
    elif [[ "$GetToken" == "curl"* ]]; then
        echo -n -e "\r discovery+:\t\t\t\tFailed (Network Connection)\n"
        return
    fi
    local Token=$(echo $GetToken | grep -o '"token" : "[^"]*' | cut -d '"' -f 4)
#    local tmpresult=$(curl -sS --max-time 6 "https://us1-prod-direct.discoveryplus.com/users/me" -b "_gcl_au=1.1.858579665.1632206782; _rdt_uuid=1632206782474.6a9ad4f2-8ef7-4a49-9d60-e071bce45e88; _scid=d154b864-8b7e-4f46-90e0-8b56cff67d05; _pin_unauth=dWlkPU1qWTRNR1ZoTlRBdE1tSXdNaTAwTW1Nd0xUbGxORFV0WWpZMU0yVXdPV1l6WldFeQ; _sctr=1|1632153600000; aam_fw=aam%3D9354365%3Baam%3D9040990; aam_uuid=24382050115125439381416006538140778858; st=${Token}; gi_ls=0; _uetvid=a25161a01aa711ec92d47775379d5e4d; AMCV_BC501253513148ED0A490D45%40AdobeOrg=-1124106680%7CMCIDTS%7C18894%7CMCMID%7C24223296309793747161435877577673078228%7CMCAAMLH-1633011393%7C9%7CMCAAMB-1633011393%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1632413793s%7CNONE%7CvVersion%7C5.2.0; ass=19ef15da-95d6-4b1d-8fa2-e9e099c9cc38.1632408400.1632406594" --connect-to us1-prod-direct.discoveryplus.com:443:$sniProxy:443 2>&1)
#    local result=$(echo $tmpresult | grep -o '"currentLocationTerritory" : "[^"]*' | cut -d '"' -f 4)

    local tmpresult=$(curl -sS --max-time 6 'https://global-prod.disco-api.com/bootstrapInfo' -H 'authority: global-prod.disco-api.com' -H 'accept: */*' -H 'accept-language: zh-CN,zh;q=0.9,en;q=0.8' -H 'cache-control: no-cache' -H 'origin: https://auth.discoveryplus.com' -H 'pragma: no-cache' -H 'referer: https://auth.discoveryplus.com/' -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36' -H 'x-disco-client: WEB:UNKNOWN:WEB_AUTH:1' -H 'x-disco-params: hn=auth.discoveryplus.com,bid=dplus' --connect-to global-prod.disco-api.com:443:$sniProxy:443 2>&1)
#    "mainTerritoryCode":"ie"
    local result=$(echo "$tmpresult" | grep -o '"mainTerritoryCode":"[^"]*' | cut -d '"' -f 4)
#    Austria, Brazil, Canada, Denmark, Finland, Germany, India, Ireland, Italy, the Netherlands, Norway, Spain, Sweden and the United Kingdom
    my_array=("at" "br" "ca" "dk" "fi" "de" "in" "ie" "it" "nl" "no" "es" "se" "gb" "us")
    for element in "${my_array[@]}"; do
        if [[ "$result" == "$element" ]]; then
            local upper_case=$(echo "$element" | tr '[:lower:]' '[:upper:]')
            echo -n -e "\r discovery+:\t\t\t\tYes (Region: [\"$upper_case\"])\n"
            return
        fi
    done

    echo -n -e "\r discovery+:\t\t\t\tFailed($result)\n"
    return
}

function MediaUnlockTest_DAZN() {
    local result=$(curl -sS --max-time 6 --location 'https://authentication-prod.ar.indazn.com/v1/validate-email' --header 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36' --header 'Content-Type: application/json' --header 'X-DAZN-UA: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 signin/4.40.3.81 hyper/0.14.0 (web; production; us)' --header 'Referer: https://www.dazn.com/' --data-raw '{"Email":"thomasamir4@gmail.com"}' --connect-to authentication-prod.ar.indazn.com:443:$sniProxy:443
 2>&1)
    if echo "$result" | grep -q "\"Forbidden\""; then
        echo -n -e "\r DAZN:\t\t\t\tNo (Forbidden)\n"
    elif echo "$result" | grep -q "\"exists\""; then
        local region=$(curl -sS --max-time 6 --location 'https://startup.core.indazn.com/misl/v5/Startup' --data '{"Version":"2","LandingPageKey":"generic","Languages":"zh-CN,zh,en","Platform":"web","Manufacturer":"","PromoCode":"","PlatformAttributes":{}}' --header 'content-type: application/json' --connect-to startup.core.indazn.com:443:$sniProxy:443
 2>&1)
        # "GeolocatedCountry":"us"
        if echo "$region" | grep -q "\"isAllowed\":true"; then
            local region2=$(echo "$region"| grep -o '"GeolocatedCountry":"[^"]*"' | cut -d'"' -f4 | grep -o '[^"]*' | tr '[:lower:]' '[:upper:]')
            echo -n -e "\r DAZN:\t\t\t\tYes (Region: [\"$region2\"])\n"
        else
            echo -n -e "\r DAZN:\t\t\t\tNo (Region blocked)\n"
        fi
    else
        echo -n -e "\r DAZN:\t\t\t\t Network Failed\n"
    fi
   
}

function MediaUnlockTest_JioCinema() {
#{
#    "code": 474,
#    "message": "Service Unavailable",
#    "error_description": "JioCinema is unavailable at your location.Learn more by visiting the link below."
#}

#{
#    "code": 419,
#    "message": "[1012] Your session has expired. Simply log in again to pick up where you left"
#}

#475是IN的ip被污染了, 无法使用

    local result_code=$(curl --max-time 6 'https://apis-jiovoot.voot.com/playbackjv/v5/3957165' -fsL --write-out %{http_code} --output /dev/null --header 'Content-Type: text/plain' --data '{"4k":false,"ageGroup":"18+","appVersion":"3.4.0","bitrateProfile":"xhdpi","capability":{"drmCapability":{"aesSupport":"yes","fairPlayDrmSupport":"yes","playreadyDrmSupport":"none","widevineDRMSupport":"yes"},"frameRateCapability":[{"frameRateSupport":"30fps","videoQuality":"1440p"}]},"continueWatchingRequired":false,"dolby":false,"downloadRequest":false,"hevc":false,"kidsSafe":false,"manufacturer":"Mac OS","model":"Mac OS","multiAudioRequired":true,"osVersion":"10.15.7","parentalPinValid":true,"x-apisignatures":"o668nxgzwff"}'  --connect-to apis-jiovoot.voot.com:443:$sniProxy:443)

    if [ "$result_code" != "419" ]; then
        echo -n -e "\r JioCinema:\t\t\t\tNo (apis-jiovoot.voot.com Error: $result_code)\n"
        return
    fi

    local result_code2=$(curl --max-time 6 'https://www.jiocinema.com/sports/cricket/csk-vs-lsg-highlights/3957165' -fsL --write-out %{http_code} --output /dev/null --connect-to www.jiocinema.com:443:$sniProxy:443)

    if [ "$result_code2" != "200" ]; then
        echo -n -e "\r JioCinema:\t\t\t\tNo (www.jiocinema.com Error: $result_code2)\n"
        return
    fi
    
    echo -n -e "\r JioCinema:\t\t\t\tYes (Region: [\"IN\"])\n"
}

function Global_UnlockTest() {
    echo ""
    echo "============[ Multination ]============"
    local result=$(
    MediaUnlockTest_DisneyPlus ${1} &
    MediaUnlockTest_Netflix ${1} &
    OpenAITest ${1} &
    MediaUnlockTest_HotStar ${1} &
    MediaUnlockTest_BBCiPLAYER ${1} &
    MediaUnlockTest_HuluUS ${1} &
    MediaUnlockTest_FuboTV ${1} &
    MediaUnlockTest_SlingTV ${1} &
    MediaUnlockTest_ABC ${1} &
    MediaUnlockTest_ESPNPlus ${1} &
#    MediaUnlockTest_Omegle ${1} &
    MediaUnlockTest_AbemaTV_IPTest ${1} &
    MediaUnlockTest_TubiTV ${1} &
    MediaUnlockTest_FOXSports ${1} &
    MediaUnlockTest_SonyLIV ${1} &
    MediaUnlockTest_TrueID ${1} &
    MediaUnlockTest_F1tvpro ${1} &
    MediaUnlockTest_Channel4 ${1} &
    MediaUnlockTest_Max ${1} &
    MediaUnlockTest_DiscoveryPlus ${1} &
    MediaUnlockTest_DAZN ${1} &
    MediaUnlockTest_JioCinema ${1} &
    )
    wait
    local array=("Disney+:" "Netflix:" "Netflix Originals:" "ChatGPT:" "Hotstar:" "BBC:" "Hulu:" "fuboTV:" "Sling TV:" "ABC:" "ESPN+:" "ABEMA:" "Tubi:" "FOX Sports:" "SonyLIV:" "TrueID:" "F1 TV:" "Channel 4:" "Max:" "discovery+:" "DAZN:" "JioCinema:")
    echo_Result ${result} ${array}
    echo "======================================="
}

function echo_Result() {
    for((i=0;i<${#array[@]};i++))
    do
        echo "$result" | grep "${array[i]}"
        sleep 0.03
    done;
}

# 测试disney+ and netflix的解锁能力
Global_UnlockTest 4

