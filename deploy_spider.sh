function install_python_lib(){
    apt update
    apt install -y python3-pip
    pip3 install logparser
    pip3 install scrapyd
    pip3 install pymongo
    pip3 install scrapy-redis
    pip3 install pycrypto
    pip3 install redis
    pip3 install bs4
    pip3 install lxml
    pip3 install PyExecJS
    pip3 install scrapy-fake-useragent
    apt install -y nodejs
    apt install -y zip
    apt install -y unzip
    wget -N --no-check-certificate -P /root  https://gitlab.com/Chanli520/downloadfile/-/raw/master/node_modules.zip
    unzip /root/node_modules.zip
    echo "install python lib successfully"
}


function setup_ssh_tunel(){

    wget -N --no-check-certificate  -P /root  https://s3.amazonaws.com/rds-downloads/rds-combined-ca-bundle.pem 
    if [ ! -d /usr/tunnel ]; then
        mkdir /usr/tunnel
        echo "create the tunnel Path Successfully"
    fi
    wget -N --no-check-certificate -P /usr/tunnel   https://gitlab.com/Chanli520/downloadfile/-/raw/master/ssh_tun.service
    wget -N --no-check-certificate -P /usr/tunnel   https://gitlab.com/Chanli520/downloadfile/-/raw/master/ssh_tunel.sh
    cp  /usr/tunnel/ssh_tun.service /lib/systemd/system/
    systemctl enable ssh_tun.service
    service ssh_tun start
    echo "create ssh tunnel successfully"
}



function setup_scrapyd(){

    pip3 install supervisor
    echo_supervisord_conf > /etc/supervisord.conf
    mkdir /root/logs
    if [ -f "/etc/supervisord.conf" ]
then
   cat >> /etc/supervisord.conf <<EOF
[program:logparser_services]
;numprocs=1
;process_name=%(program_name)s
command=logparser
autostart=true
user=root
autorestart=true
startsecs = 5
redirect_stderr=true
loglevel=info
stderr_logfile=/var/log/logparser_services.err.log
stdout_logfile=/var/log/logparser_services.out.log

[program:scrapyd_services]
;numprocs=1
;process_name=%(program_name)s
command=scrapyd
autostart=true
user=root
autorestart=true
startsecs = 5
redirect_stderr=true
loglevel=info
stderr_logfile=/var/log/scrapyd_services.err.log
stdout_logfile=/var/log/scrapyd_services.out.log
EOF
  echo "Install supervisord Success"

else
  echo "Install supervisord Error"
fi

supervisord -c /etc/supervisord.conf
supervisorctl reload
supervisorctl restart all
echo "setup_scrapyd successfully"

}


function set_nginx_auth(){

    apt install -y nginx
    apt install -y apache2-utils
    cat > /etc/nginx/conf.d/.htpasswd  <<EOF
lightsail:\$apr1\$rcqkHMEe\$oh0bXbwpCxAJx9mh.QVQQ.
EOF
    cat > /etc/nginx/conf.d/scrapy.conf  <<EOF
server {
        listen 6801;
        client_max_body_size 200m;
        location / {
                proxy_pass            http://127.0.0.1:6800/;
                auth_basic            "Restricted";
                auth_basic_user_file  /etc/nginx/conf.d/.htpasswd;
        }
}
EOF
    echo "set nginx auth successfully"
    service nginx reload
    echo "restart nginx successfully"

}

function install_supervisord_service() {
       cat >> /lib/systemd/system/crawl.service <<EOF
[Unit]
Description=Supervisor daemon

[Service]
Type=forking
ExecStart=/usr/local/bin/supervisord -c /etc/supervisord.conf
ExecStop=/usr/local/bin/supervisorctl shutdown
ExecReload=/usr/local/bin/supervisorctl reload
KillMode=process
Restart=on-failure
RestartSec=42s

[Install]
WantedBy=multi-user.target
EOF
systemctl enable  crawl
supervisorctl shutdown
service crawl start
service crawl status
}


install_python_lib
setup_scrapyd
set_nginx_auth
#setup_ssh_tunel  去掉建立隧道链接
install_supervisord_service
