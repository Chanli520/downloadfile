upgrade_control(){

     pushd /LightsailVpn/backend/Vpn/Server/
     git pull
}



clean_old_services(){
     service ss stop 
     service ss_2 stop
     echo "stop all ss service"
     ps -aux  |grep ss-server
     rm  /usr/vpn_service/ss_2.sh
     echo "del ss_2.sh"
     rm  /usr/vpn_service/ss.sh
     echo "del ss.sh"
     rm  /lib/systemd/system/ss_2.service
     echo "del ss_2.service"
     rm /lib/systemd/system/ss.service
     echo "del ss.service"
     systemctl daemon-reload
     echo "reload service"
}



create_new_services(){

     cp /LightsailVpn/backend/Vpn/Server/ss_2.sh /usr/vpn_service/
     echo "create ss_2.sh success"
     cp  /LightsailVpn/backend/Vpn/Server/ss_2.service  /lib/systemd/system/
     echo "create ss_2.sh success"
     systemctl daemon-reload
     echo "reload service"
     systemctl enable ss_2.service
     echo "set auto restart ss_2 service"

}


start_new_service(){
     cp /var/run/proxy.config  /etc/
     echo "create proxy.config in /etc/"
     service ss_2 restart
     echo "sleep 3 s"
     sleep 3
     service ss_2 status
     supervisorctl reload
     echo "reload benchmark,sleep 3 s"
     sleep 3
     supervisorctl status
     echo "update new service success"
}

upgrade_control     #更新代码
clean_old_services  #清除旧版的服务
create_new_services #创建新的服务
start_new_service   #开启新的服务