

# 创建公钥

check_keys_nums(){
  cat "$HOME/.ssh/authorized_keys" | grep "AAAAB3NzaC1yc2EAAAADAQABAAABAQCwXfMUaFueE6byQJx6z3anzl/Tbunf2LE0EbkWaUESwGJJc2A5FD9FIA/mT1yNptPHXwIWEVP2LmsZEZhMSu/GQyNIFFroBwBcTdQw9uQOGleD3f3vVqjSlPpP5roNVFV8cRuUpDn+s3PV8eU/sLnHay0Cjr/lFL1gK1qCtFWbYpUMllUgZgJqYm6N0S+6HAE+AAFmnqjsv+cnNRGZSlQikui+T0dCQvq3F5Qc8iMxIS5RN7pIhrUPKII7j2FITeVvvT9WOlfZNYeFA9zx3GqnugBV7nToKyElU+s3gQbvomvY+zVw42/dyNEAbU7cRQrPOq9PVfioQoaLkdHrYAPF"  | wc -l
}

append_keys(){
  cat >> "$HOME/.ssh/authorized_keys" <<EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCwXfMUaFueE6byQJx6z3anzl/Tbunf2LE0EbkWaUESwGJJc2A5FD9FIA/mT1yNptPHXwIWEVP2LmsZEZhMSu/GQyNIFFroBwBcTdQw9uQOGleD3f3vVqjSlPpP5roNVFV8cRuUpDn+s3PV8eU/sLnHay0Cjr/lFL1gK1qCtFWbYpUMllUgZgJqYm6N0S+6HAE+AAFmnqjsv+cnNRGZSlQikui+T0dCQvq3F5Qc8iMxIS5RN7pIhrUPKII7j2FITeVvvT9WOlfZNYeFA9zx3GqnugBV7nToKyElU+s3gQbvomvY+zVw42/dyNEAbU7cRQrPOq9PVfioQoaLkdHrYAPF
EOF
}

write_ssh_key(){
echo "Start"
if [ ! -d "$HOME/.ssh" ]
then
echo "create ssh path"
mkdir "$HOME/.ssh"
else
echo "ssh had exists" 
fi

if [ ! -f "$HOME/.ssh/authorized_keys" ]
then
echo "create authorized_keys"
touch "$HOME/.ssh/authorized_keys"
chmod 600 "$HOME/.ssh/authorized_keys"
echo "append rsa public key "
append_keys

else
key_Nums=`check_keys_nums`
if [[ ${key_Nums} == 0 ]]
then
    echo "append rsa public key "
    append_keys
else
  echo "had append keys  finished"
fi
fi
echo "Finished"
}

write_ssh_key