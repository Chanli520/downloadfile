#!/bin/bash

setup_auto_supervisor(){
  supervisorctl stop all
if [ ! -f "/lib/systemd/system/supervisor.service" ]
then
   cat > /lib/systemd/system/supervisor.service  <<EOF
[Unit]
Description=supervisor
After=network.target

[Service]
Type=forking
ExecStart=/usr/local/bin/supervisord -c /etc/supervisord.conf
ExecStop=/usr/local/bin/supervisorctl \$OPTIONS shutdown
ExecReload=/usr/local/bin/supervisorctl \$OPTIONS reload
KillMode=process
Restart=on-failure
RestartSec=42s

[Install]
WantedBy=multi-user.target
EOF
  chmod 766 /lib/systemd/system/supervisor.service
  systemctl enable supervisor.service
  systemctl daemon-reload
  killall -9 supervisord
  service supervisor restart
  echo "Install supervisord services Success"
else
  echo "had installed supervisord services"
fi
}

write_new_supervisor_conf(){
   cat > /etc/supervisord.conf << EOF
[unix_http_server]
file=/tmp/supervisor.sock   ; the path to the socket file

[supervisord]
logfile=/tmp/supervisord.log ; main log file; default $CWD/supervisord.log
logfile_maxbytes=50MB        ; max main logfile bytes b4 rotation; default 50MB
logfile_backups=10           ; # of main logfile backups; 0 means none, default 10
loglevel=info                ; log level; default info; others: debug,warn,trace
pidfile=/tmp/supervisord.pid ; supervisord pidfile; default supervisord.pid
nodaemon=false               ; start in foreground if true; default false
silent=false                 ; no logs to stdout if true; default false
minfds=1024                  ; min. avail startup file descriptors; default 1024
minprocs=200                 ; min. avail process descriptors;default 200

[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

[supervisorctl]
serverurl=unix:///tmp/supervisor.sock ; use a unix:// URL  for a unix socket

[include]
files = /etc/supervisor_configs/*.conf
EOF

}


setup_supervisor(){
  echo "setup supervisor"
  if [ -f "/etc/supervisord.conf" ];then
    echo "had installed supervisor"
  else
    pip3 install supervisor==4.2.2
    ehco "install supervisor success"
fi
  if [ ! -d "/etc/supervisor_configs" ];then
    echo "create new supervisor_configs dir"
    mkdir /etc/supervisor_configs
  else
    echo "supervisor_configs dir had exists"
fi
  echo "rewrite supervisor.conf"
  write_new_supervisor_conf
  supervisord -c /etc/supervisord.conf
  setup_auto_supervisor
}

install_lib(){
    echo "install libs"
    apt update
    apt install -y python3-dev
    apt install -y gcc
    apt install -y psmisc
    apt install -y iptables
    apt install -y vim
    apt install -y lrzsz
    apt install -y python3-pip
    apt install -y git
    apt install -y wget
    apt install -y net-tools
    apt install -y htop
    pip3 install setuptools
}


download_sniproxy() {
  echo  "download sniproxy"
  pushd /
  wget https://gitlab.com/Chanli520/downloadfile/-/raw/master/sniproxy.tar.gz
  tar -zxvf sniproxy.tar.gz
  chmod +x /sniproxy/sniproxy
  mkdir /var/log/sniproxy
  cp /sniproxy/sni.conf /etc/supervisor_configs
  sleep 2  
  supervisorctl update
  sleep 2
  supervisorctl status
}




# Function to check if any file in /etc/supervisor_configs contains "443" in its name
check_for_443() {
    local file_found=0
    
    # Iterate through each file in /etc/supervisor_configs
    for file in /etc/supervisor_configs/*; do
        # Check if the filename contains "443"
        if [[ "$file" == *443* ]]; then
            file_found=1  # File containing "443" found
            break
        fi
    done
    
    echo "$file_found"
}



install(){
    echo "install sniproxy"
    # Check if the file or directory exists
    if [ -e "/LightsailVpn/shadowsocks-libev" ]; then
        echo "shadowsocks exists"
        download_sniproxy
    else
        echo "shadowsocks not exist"
        install_lib
        setup_supervisor
        download_sniproxy
    fi
    echo "install sniproxy finished"
}

# Call the function and store its return value
check_for_443_result=$(check_for_443)

# Check the return value and print appropriate message
if [ "$check_for_443_result" -eq 1 ]; then
    echo "443 port in use"
    echo "install sniproxy failed"
elif [ "$check_for_443_result" -eq 0 ]; then
    install
else
    echo "Invalid Check return 443 value: $check_for_443_result"
fi






