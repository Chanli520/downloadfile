import requests,subprocess

def get_all_hosts():
    url = "http://monitor.lightsailvpn.com/manager/v2/configs?token=Lightsail"
    try:
        res = requests.get(
            url=url,
            timeout=10
        )
        config_list = res.json()["config_list"]
        hosts = set()
        for config  in config_list:
            ip  = config["sourceip"]
            hosts.add(ip)
        return list(hosts)
    except Exception as e:
        print(e)
        return []

def get_all_cmd():
    all_hosts = get_all_hosts()
    cmds = []
    for ip in all_hosts:
        cmd = f'curl --interface {ip} https://www.google.com -s | grep -o  "lang=\\"[a-zA-Z\-]*\\""'
        cmds.append({"cmd":cmd,"ip":ip})
    return cmds


def execute_cmd(cmd,ip):
    '''
    :param cmd:
    :return:
    '''
    try:
        result = None
        return_code = -1
        P = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, universal_newlines=True)
        return_code = P.wait(timeout=15)
        if return_code != 0:
            print(f"execute :{cmd} error!")
        outputs = P.stdout.readlines()
        result = "".join(outputs)
    except Exception as e:
        print(f"execute :{cmd} error!")
    finally:
        P.kill()
    return {'result': f"{ip}:{result}", 'code': return_code}


def upload_msg(msg):
    try:
        res = requests.get(
            url = f"http://139.162.20.118:7000/google/address?msg={msg}",
            timeout=10
        )
    except Exception as e:
        print("上传异常",e,msg)


for items in get_all_cmd():
    cmd = items["cmd"]
    ip = items["ip"]
    rs =  execute_cmd(cmd=cmd,ip=ip)["result"]
    upload_msg(msg=rs)
    print(rs)

